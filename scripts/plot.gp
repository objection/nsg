#!/bin/env gnuplot

set terminal png # or set terminal svg or set terminal pdf for different output formats
set output "/tmp/nsg-samples.png"  # Output filename and format

set xlabel "Time"
set ylabel "Amplitude"

plot "/tmp/nsg-samples.txt" every 256 using 1:2 with lines title "NSG samples"
