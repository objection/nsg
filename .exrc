vim9script

import autoload 'compile_and_run.vim' as car

car.Register(expand("<script>:p:h"), 'test.nml')

def Create_parser_def()
	var line =<< trim END
	static int TEMP_NAME (ctx *ctx, block_stack *bs) {
		int r = 1;
		auto bt = TOP (bs);


		r = 0;
	out:
		return r;
	}
	END
	append(line('.'), line)
enddef
command -nargs=0 Createparserdef Create_parser_def()
nnoremap <leader>cp :Createparserdef<cr>

