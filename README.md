# What

It's a synth and "NML" parser, and a program for playing tunes got
from "NML" files. NML is just what I'm calling the type of MML I've
invented. I haven't going to stick slavishly to the standard. There's
no MML standard, anyway.

# Structure

The synth and NML bit is most of the program, and it's compiled as a
library. The main.c file is the program, and it links to the library.
If you wanted to use the library, just meson install and link with it
in your program. It should be that simple, but probably won't be.
`#include <nml.h>` in your program.

# Using the library

Create a nsg_tune with nsg_get_tune_from_nml, then, in your audio
callback (or whatever; there's other ways of doing audio), fill a
buffer (of floats) with nsg_fill_tunes_samples, and that should be it.
As for writing the NML, I suppose at some point I'll document it. So,
just don't use this program, not yet.

# Acknowledgements

* I use Felipe Ferreira da Silva's mathc library, the easing functions.
* The Audio Programming book. All the audio stuff comes from there.
  And ChatGPT.
