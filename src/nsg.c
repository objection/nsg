#define _GNU_SOURCE
#include "under-nsg.h"
#include "filters.h"

static inline float get_normalised_event_distance (nsg_event_s *a, nsg_event_s *b,
		float elapsed_time) {
	return (elapsed_time - a->start) / (b->start - a->start);
}

static float lerp_between_float_events (nsg_ctx_s *ctx, nsg_event_s *a, nsg_event_s *b,
		float frame_idx, nsg_easing_fn *fn) {

	// You've forgotten the lerp-to event.
	NSG_ASSERT (b);
	return lerp (a->param.val, b->param.val, fn (get_normalised_event_distance (a, b,
				frame_idx), ctx->fns));
}

static float lerp_between_tick_events (nsg_ctx_s *ctx, nsg_event_s *this_tick,
		nsg_event_s *next_tick, nsg_table_s *this_table,
		nsg_table_s *next_table, float elapsed_time, nsg_osc_state_s *this_osc_state,
		nsg_osc_state_s *lerp_to_osc_state) {
	float a = this_tick->param.tick.fn (ctx->fns, this_osc_state, this_table);
	float b = next_tick->param.tick.fn (ctx->fns, lerp_to_osc_state, next_table);
	float dist =
			get_normalised_event_distance (this_tick, next_tick,
				elapsed_time);
	return lerp (a, b, dist);
}

static void update_maths_osc_state (nsg_ctx_s *ctx, nsg_osc_state_s *osc_state, float freq,
		nsg_table_s *) {
	osc_state->angle_incr = ctx->two_pi_over_sr * freq;
	osc_state->phase += osc_state->angle_incr;
	if (osc_state->phase >= NSG_TWO_PI)
		osc_state->phase -= NSG_TWO_PI;
	if (osc_state->phase < 0.0f)
		osc_state->phase += NSG_TWO_PI;
}

static void update_table_osc_state (nsg_ctx_s *ctx, nsg_osc_state_s *osc_state, float freq,
		nsg_table_s *table) {
	osc_state->index += freq * table->n_samples / ctx->sample_rate;

	while (osc_state->index >= table->n_samples)
		osc_state->index -= table->n_samples;
	while (osc_state->index < 0)
		osc_state->index += table->n_samples;
}

static void (*update_osc_state_fns[2]) (nsg_ctx_s *ctx, nsg_osc_state_s *,
		float freq, nsg_table_s *) = {
	[NTT_MATHS] = update_maths_osc_state,
	[NTT_TABLE] = update_table_osc_state,
};

static float get_sample_val (nsg_ctx_s *ctx, nsg_event_s *this_event, nsg_event_s *next_event,
		u32 frame_idx) {
	float r;

	switch (this_event->op.id) {
		case NOT_STRAIGHT_VAL:
			r = this_event->param.freq;
			break;
		case NOT_INTERP: {
			auto fn = this_event->op.easing_fn;
			r = lerp_between_float_events (ctx, this_event, next_event, frame_idx, fn);
		}
			break;
		case NOT_OSC: {

			NSG_ASSERT (!"Gone for now");
			// Here we change the event's state. This is not now it is
			// when we do normal channels. I don't like it.

			/* auto osc = &this_event->param.osc; */
			/* r = osc->amp * osc->tick (nsg_ctx->fns, &osc->state); */
			/* update_osc_state_fns[osc->state.type] (nsg_ctx, &osc->state, osc->freq); */
		}
			break;
	}
	return r;
}

static float get_tick_sample (nsg_ctx_s *ctx, nsg_event_s *this_tick,
		nsg_event_s *lerp_to_tick_event, nsg_table_s *this_table,
		nsg_table_s *lerp_to_table, float elapsed_time,
		nsg_osc_state_s *this_osc_state, nsg_osc_state_s *lerp_to_osc_state) {
	float r;
	if (!(this_tick->op.id == NOT_INTERP))
		r = this_tick->param.tick.fn (ctx->fns, this_osc_state, this_table);
	else
		r = lerp_between_tick_events (ctx, this_tick, lerp_to_tick_event,
				this_table, lerp_to_table,
				elapsed_time, this_osc_state, lerp_to_osc_state);
	return r;
}

static inline void set_param (channel_param_state_s *cps, nsg_event_s *event,
		nsg_event_s *next_event) {

#define set_it(this, next) \
	do { \
		this = event; \
		if (event->op.id == NOT_INTERP) { \
			NSG_ASSERT (next_event && next_event->param.type == (this)->param.type); \
			next = next_event; \
		} \
	} while (0)

	switch (event->param.type) {
		case NPT_TICK:
			set_it (cps->this_tick, cps->next_tick);
			break;
		case NPT_TABLE:
			set_it (cps->this_table, cps->next_table);
			break;
		case NPT_FREQ:
			set_it (cps->this_freq, cps->next_freq);
			break;
		case NPT_AMP:
			set_it (cps->this_amp, cps->next_amp);
			break;
		case NPT_IIR_LP_FILTER:
			set_it (cps->this_lp, cps->next_lp);
			break;
		case NPT_IIR_HP_FILTER:
			set_it (cps->this_hp, cps->next_hp);
			break;
		case NPT_RESONATING_FILTER:
			set_it (cps->this_res, cps->next_res);
			break;
		case NPT_BP_FILTER:
			set_it (cps->this_bp, cps->next_bp);
			break;
	}
}

static inline void update_channel_param_state (nsg_channel_s *channel,
		u32 tune_frame_idx) {

	auto cps = &channel->param_state;

	// Increment the idx if we're >= it's start time or
	// we're at the first one. Also set the channel state events.
	each (stream, channel->n_streams, channel->streams) {

		if (!stream->n)
			continue;

		// Don't try to put this set_param call outside of these
		// branches. The point is we only do it (set the new param
		// state) when we're moving between events.
		if (tune_frame_idx == 0)
			set_param (cps, &stream->d[0],
					stream->idx < stream->n - 1 ? &stream->d[stream->idx + 1] : 0);

		else if (stream->idx < stream->n - 1 &&
				tune_frame_idx >= stream->d[stream->idx + 1].start) {

			// Next event starts; incr idx.
			stream->idx++;
			set_param (cps, &stream->d[stream->idx],
					stream->idx < stream->n - 1 ? &stream->d[stream->idx + 1] : 0);

		}
	}
}

static inline void apply_filter (nsg_ctx_s *ctx, nsg_filter_state_s *filter_state,
		nsg_filter_params_s *params, float *audio_sample) {
	if (params->fn)
		params->fn (ctx, filter_state, params, audio_sample);
}

static inline void write_channels_sample (nsg_ctx_s *ctx, nsg_channel_s *channel,
		u32 tune_frame_idx, float *out) {

	update_channel_param_state (channel, tune_frame_idx);

	auto cps = &channel->param_state;
	float freq = get_sample_val (ctx, cps->this_freq, cps->next_freq,
			tune_frame_idx);
	float amp = get_sample_val (ctx, cps->this_amp, cps->next_amp,
			tune_frame_idx);

	auto this_table = cps->this_table ? &cps->this_table->param.table : 0;
	auto next_table = cps->next_table ? &cps->next_table->param.table : 0;

	NSG_ASSERT (this_table == 0 || this_table > (nsg_table_s *) 1000);

	float audio_sample = amp * get_tick_sample (ctx, cps->this_tick,
			cps->next_tick, this_table, next_table, tune_frame_idx,
			&channel->osc_state, &channel->lerp_to_osc_state);

	float unfiltered_audio_sample = audio_sample;

	apply_filter (ctx, &channel->iir_lp_filter_state,
			&channel->param_state.this_lp->param.filter_params, &audio_sample);
	apply_filter (ctx, &channel->iir_hp_filter_state,
			&channel->param_state.this_hp->param.filter_params, &audio_sample);
	apply_filter (ctx, &channel->resonating_filter_state,
			&channel->param_state.this_res->param.filter_params, &audio_sample);
	apply_filter (ctx, &channel->bp_filter_state,
			&channel->param_state.this_bp->param.filter_params, &audio_sample);

	auto balance_filter_state = &channel->balance_filter_state;
	balance_filter (ctx, balance_filter_state,
			unfiltered_audio_sample, 8000.0f, &audio_sample);

	// It's fine to increment this here. Also, you'll see that
	// the two parts of lerps share osc state. Probably fine.
	// Probably should experiment.
	update_osc_state_fns[cps->this_tick->param.tick.type] (ctx, &channel->osc_state,
			freq, this_table);
	if (cps->this_table && cps->this_table->op.id == NOT_INTERP)
		update_table_osc_state (ctx, &channel->lerp_to_osc_state, freq,
				next_table);

	// Write at the very end.
	*out += 1.0f * audio_sample;
}

[[nodiscard]] int nsg_fill_tunes_samples (float two_pi_over_sr, float sample_rate,
		nsg_fns_s fns, nsg_tune_s *tune, int n_samples, float *samples) {
	nsg_ctx_s ctx = {
		.two_pi_over_sr = two_pi_over_sr,
		.fns = &fns,
		.sample_rate = sample_rate
	};

	each (channel, tune->n_channels, tune->channels) {
		for (int i = 0; i < n_samples; i++)
			write_channels_sample (&ctx, channel, tune->frame_idx + i, &samples[i]);
	}
	tune->frame_idx += n_samples;

	return 0;
}

int nsg_free_tune (nsg_tune_s *tune, nsg_allocator_s *allocator) {
	for (ssize_t i = 0; i < tune->n_channels; i++) {
		for (ssize_t j = 0; j < tune->channels[i].n_streams; j++)
			allocator->free (tune->channels[i].streams[j].d, allocator->ctx);
	}
	allocator->free (tune->channels, allocator->ctx);
	return 0;
}

#if 0
// This function exists so we can set that two_pi_over_sr variable,
// which is needed to increment the phase. struct nsg does have the
// sample rate and so could calculate it each time but best not.
int set_nsg (int n_tunes, nsg_tune **tunes, int sample_rate,
		int n_samples, nsg *out) {
#if 0
	*out = (nsg) {
		.n_tunes = n_tunes,
		.tunes = tunes,
		.n_samples = n_samples,
		.sample_rate = sample_rate,
	};
#endif
	return 0;
}
#endif

[[nodiscard]] bool nsg_you_have_all_maths_fns (nsg_fns_s *fns) {

	for (size_t i = 0; i < sizeof *fns / sizeof fns->acos; i++) {
		void *whatever = &(*fns) + i * sizeof fns->acos;
		if (!whatever)
			return 0;
	}
	return 1;
}
