#define _GNU_SOURCE

#include <argp.h>
#include <SDL2/SDL.h>
#include <err.h>
#include <readline/readline.h>
#include <readline/history.h>
#include "nsg.h"

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

static int DEFAULT_BUFFER_SIZE = 2056;
static int DEFAULT_SAMPLE_RATE = 48000;
static char *SAMPLES_FILE_PATH = "/tmp/nsg-samples.txt";
static FILE *SAMPLES_F;

enum args_flags {
	AF_STR = 1 << 0,
	AF_REPL = 1 << 1,
};
typedef enum args_flags args_flags_e;

typedef struct args args_s;
struct args {

	args_flags_e flags;
	union {
		char **tune_paths;
		nsg_tune_s **tunes;
	};
	int n_tunes;
	int buffer_size;
	int sample_rate;
	float two_pi_over_sr_;
};

static void *nsg_sdl_malloc (size_t size, void * /* ctx */) {
	return SDL_malloc (size);
}

static void nsg_sdl_free (void *ptr, void * /* ctx */) {
	SDL_free (ptr);
}

static void *nsg_sdl_realloc (void *ptr, size_t size, void * /* ctx */) {
	return SDL_realloc (ptr, size);
}

static nsg_allocator_s SDL_ALLOCATOR = {
	.free = nsg_sdl_free,
	.malloc = nsg_sdl_malloc,
	.realloc = nsg_sdl_realloc
};

static nsg_fns_s SDL_FNS = {
	.powf = SDL_powf,
	.sinf = SDL_sinf,
	.cosf = SDL_cosf,
	.fabsf = SDL_fabsf,
	.sqrtf = SDL_sqrtf,

	// Get rid of this if you determine it doesn't matter. The filter
	// function uses doubles for a couple of values. I don't know how
	// necessary it is.
	.sin = SDL_sin,
	.cos = SDL_cos,
	.sqrt = SDL_sqrt,
	.acos = SDL_acos,
};

static error_t parse_arg (int key, char *arg, struct argp_state *state) {

	args_s *args = state->input;

	switch (key) {
		case 'r':
			if (SDL_sscanf (arg, "%d", &args->sample_rate) != 1)
				err (1, "%s isn't a valid int", arg);
			break;
		case 'b':
			if (SDL_sscanf (arg, "%d", &args->buffer_size) != 1)
				err (1, "%s isn't a valid int", arg);
			break;
		case 'R':
			args->flags |= AF_REPL;
			break;
		case 's':
			args->flags |= AF_STR;
			break;
		case ARGP_KEY_INIT:
			break;
		case ARGP_KEY_ARG:
			args->tunes = SDL_realloc (args->tunes,
					sizeof *args->tune_paths * (args->n_tunes + 1));
			args->tune_paths[args->n_tunes++] = arg;
			break;
		case ARGP_KEY_END:
			if (!(args->flags & AF_REPL) && !args->n_tunes)
				argp_usage (state);
			break;
		default:
			break;
	}

	return 0;
}

static void audio_callback (void *user_data, u8 *u8_stream, int len_in_bytes) {

	args_s *args = user_data;
	nsg_tune_s *tune = args->tunes[0];

	// You absolutely have to memset.
	SDL_memset (u8_stream, 0, len_in_bytes);
	float *float_stream = (float *) u8_stream;
	(void) nsg_fill_tunes_samples (args->two_pi_over_sr_, args->sample_rate, SDL_FNS, tune,
			len_in_bytes / sizeof (float), float_stream);
#if 0
	bool got_one = 0;
	for (int i = 0; i < args->buffer_size; i++) {
		if (float_stream[i] > 0.5f || float_stream[i] < -0.5f) {
			printf ("%f, ", float_stream[i]);
			got_one = 1;
		}
		fprintf (SAMPLES_F, "%f %f\n", (tune->frame_idx + i) / 44100.0,  float_stream[i]);
	}
	if (got_one)
		putchar ('\n');
#endif
}

static u32 init_sdl_audio (args_s *args) {
    SDL_AudioSpec desired_spec = {
		.freq = args->sample_rate,
		.format = AUDIO_F32SYS,
		.channels = 1,
		.samples = args->buffer_size,
		.callback = audio_callback,
		.userdata = args,
	};
	SDL_AudioSpec obtained_spec;

    u32 r = SDL_OpenAudioDevice (0, 0, &desired_spec, &obtained_spec, 0);
    if (r == 0) {
        err (1, "Couldn't open audio: %s\n", SDL_GetError());
        SDL_Quit();
        return -1;
    }

	return r;
}

static int play (u32 audio_device_idx) {
	int r = 1;
    SDL_PauseAudioDevice (audio_device_idx, 0);

	union SDL_Event event;
	for (;;) {
		while (SDL_PollEvent (&event)) {
			switch (event.type) {
				case SDL_QUIT:
					goto out;
				default:
					break;
			}
		}
	}
	r = 0;
out:
    SDL_PauseAudioDevice (audio_device_idx, 1);
	return r;
}

static int repl (args_s *args, u32 audio_device_idx) {
	int r = 1;
	for (;;) {
		char *resp = readline ("? ");
		if (!resp)
			goto ok;
		if (!*resp)
			continue;

		char *p = resp;
		while (*p && isspace (*p))
			p++;

		add_history (p);

		if (*p == ',') {
			p++;
			if (!strcmp (p, "load"))
				SDL_assert (0);
			else if (!strcmp (p, "quit"))
				goto out;
		}

		nsg_tune_s *tune = nsg_get_tune_from_nml (p, "REPL TUNE", args->sample_rate,
				SDL_FNS, &SDL_ALLOCATOR);
		if (!tune)
	 		goto end;

		args->tunes = SDL_ALLOCATOR.realloc (args->tunes, sizeof (typeof (*args->tunes)) *
				++args->n_tunes, SDL_ALLOCATOR.ctx);
		args->tunes[args->n_tunes - 1] = tune;
		play (audio_device_idx);
end:
		free (resp);
	}
ok:
	r = 0;
out:
	return r;
}

static void atexit_function () {
	fclose (SAMPLES_F);
}

int main (int argc, char **argv) {
	struct argp argp = {
		.options = (struct argp_option[]) {
			{"str", 's', 0, 0, "\
\"NML-FILES\" are nml strings, not filenames"},
			{"repl", 'R', 0, 0, "Run a repl"},
			{"sample-rate", 'r', "INT", 0, "Specify the sample rate"},
			{"buffer-size", 'b', "INT", 0, "Set the buffer size"},
			{},
		},
		parse_arg,
		"NML-FILES ...", "\
A synth. You can program it with \"NML\" files, that is to say something like MML"
	};

	args_s args = {
		.buffer_size = DEFAULT_BUFFER_SIZE,
		.sample_rate = DEFAULT_SAMPLE_RATE,
	};

	SAMPLES_F = fopen (SAMPLES_FILE_PATH, "w");
	if (!SAMPLES_F)
		err (1, "Couldn't open the samples file for writing");
	atexit (atexit_function);

	int rc;
	rc = SDL_Init (SDL_INIT_AUDIO);
	if (rc)
		err (1, "Couldn't initialise SDL: %s", SDL_GetError ());

	argp_parse (&argp, argc, argv, 0, 0, &args);

	args.two_pi_over_sr_ = NSG_TWO_PI / args.sample_rate;

	auto audio_device_idx = init_sdl_audio (&args);
	if (args.flags & AF_REPL)
		exit (repl (&args, audio_device_idx));

	for (int i = 0; i < args.n_tunes; i++) {
		size_t n_str;
		if (!(args.flags & AF_STR)) {
			char *nml_str = SDL_LoadFile (args.tune_paths[i], &n_str);
			if (!nml_str)
				err (1, "Couldn't read in %s: %s", args.tune_paths[i], SDL_GetError ());
			args.tunes[i] = nsg_get_tune_from_nml (nml_str, args.tune_paths[i],
					args.sample_rate, SDL_FNS, &SDL_ALLOCATOR);
		} else
			args.tunes[i] = nsg_get_tune_from_nml (args.tune_paths[i], 0, args.sample_rate,
					SDL_FNS, &SDL_ALLOCATOR);
		if (!args.tunes[i])
			exit (1);
	}

	play (audio_device_idx);

	for (int i = 0; i < args.n_tunes; i++)
		nsg_free_tune (args.tunes[i], &SDL_ALLOCATOR);

	SDL_Quit ();

	exit (0);
}



