#define _GNU_SOURCE
#include "misc.h"

static inline char **get_str_member (void *it, void *) {
	return (char **) it;
}

struct bar_getter default_getter = {
	.fn = get_str_member,

	// I have a feeling this is a bad idea.
	.type_size = sizeof (char *),
};

int n_matching (char *a, char *b) {
	int r = 0;
	for (int i = 0; a[i]; i++) {
		if (a[i] == b[i])
			r++;
		else
			break;
	}
	return r;
}

// Taken from 2023-11-28 Rab commit.
int get_completion_idxes (int *pr, int *n_pr, char *query, int n_objs,
		void *objs, bar_getter_s *getter) {

	int max_wanted = *n_pr != -1 ? *n_pr : n_objs;

	if (!getter)
		getter = &default_getter;

	if (!objs)
		return 1;

	if (!query || !*query)
		return 1;

	*n_pr = 0;

	int longest = 0;

	for (int i = 0; i < n_objs; i++) {

		void *obj = objs + (i * getter->type_size);
		char **str = getter->fn (obj, getter->user);

		if (!str || !*str || !**str)
			continue;

		// There's a fair bit of strlening and memcmping here.

		int query_len = nsg_strlen (query);
		if (nsg_memcmp (*str, query, query_len))
			continue;

		// A full match; we exit. This means that if your query is
		// "cheese", and there's "cheese" and "cheese-monster", you'll
		// match "cheese".
		if (nsg_strlen (*str) == query_len) {
			*n_pr = 1;
			*pr = i;
			return 0;
		}

		int n_initial_matching = n_matching (*str, query);
		if (!n_initial_matching)
			continue;
		if (n_initial_matching > longest) {
			*n_pr = 1;
			pr[0] = i;
			longest = n_initial_matching;
		} else if (n_initial_matching == longest)
			pr[(*n_pr)++] = i;
	}

	if (max_wanted < *n_pr)
		*n_pr = max_wanted;

	return 0;
}

char *get (void *items, int which, bar_getter_s *getter) {
	char **r = getter->fn (items + (which * getter->type_size),
			getter->user);
	return *r;
}

int get_single_completion_match (char *query, int n_items, void *items,
		bar_getter_s *getter, char *item_kind_name, err_buf err_buf, int *out) {
	NSG_ASSERT (n_items);
	int n_matches = -1;
	int matches[n_items];
	int rc = get_completion_idxes (matches, &n_matches, query, n_items,
			items, getter);
	NSG_ASSERT (!rc);

	if (n_matches > 1) {
		int pos = snprintf (err_buf, ERR_BUF_LEN, "Too many %ss match \"%s\": ",
				item_kind_name, query);
		for (int i = 0; i < n_matches; i++) {
			pos += snprintf (err_buf + pos, ERR_BUF_LEN - pos, "\"%s\"",
					get (items, i, getter));
			if (i != n_matches - 1)
				pos += snprintf (err_buf + pos, ERR_BUF_LEN - pos, ", ");
		}
		return 1;
	}

	else if (!n_matches) {
		char *str = get (items, 0, getter);

		// One day in the far future you could make this show the
		// type. In that case this function would need to be in nml.c.
		int pos = snprintf (err_buf, ERR_BUF_LEN, "No %ss match %s. It takes: \"%s\"",
				item_kind_name, query, str);

		for (int i = 1; i < n_items; i++)
			pos += snprintf (err_buf + pos, ERR_BUF_LEN - pos, ", \"%s\"",
					get (items, i, getter));

		return 1;
	}

	*out = matches[0];
	return 0;
}

bool char_in_str_is_escaped (char *str, int pos) {
	int j = pos - 1;
	int n_backslashes = 0;

	// Count preceding backslashes.
	while (j >= 0 && str[j] == '\\') {
		++n_backslashes;
		--j;
	}

	// If there's an odd number of backslashes it's escaped.
	if (n_backslashes % 2)
		return 1;

	return 0;
}
