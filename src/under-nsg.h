#pragma once

#include "nsg.h"
#include <stdio.h>

#define NSG_ASSERT assert
#define IMPOSSIBLE NSG_ASSERT (!"Impossible")
static const float SEMITONE_RATIO = 1.059463f;

// Got this from SDL.
static const float NSG_FLT_EPSILON = 1.1920928955078125E-07f;
#define A4 440.0f
#define $n(x) ((sizeof(x)/sizeof((x)[0])) / ((size_t)(!(sizeof(x) % sizeof((x)[0])))))
static const float NSG_PI = 3.14159265358979323846;	/* pi */
static const float NSG_PI_2 = 1.57079632679489661923;	/* pi/2 */
#define stringify(x) #x
#define MAX(a,b) 																		\
	({ __typeof__ (a) _a = (a); 														\
	 __typeof__ (b) _b = (b); 															\
	 _a > b? _a : _b; })
#define MIN(a,b) \
	({ __typeof__ (a) _a = (a); \
	 __typeof__ (b) _b = (b); \
	 _a > b? _b : _a; })
#define each(_item, _n, _arr) \
	for (__typeof__ (_arr[0]) *(_item) = (_arr); (_item) < (_arr) + (_n); (_item)++)
#define grow(p, _n, _a, _incr_amount, allocator) \
	if ((_n) >= (_a)) { \
		(_a) += (_incr_amount); \
		p = (allocator).realloc (p, (_a) * sizeof (typeof (*p)), (allocator).ctx); \
		NSG_ASSERT (p); \
	}
enum {
	MAX_NAME = 64,
	ERR_BUF_LEN = 1024
};

extern nsg_allocator_s nsg_libc_allocator;
#ifdef _MATH_H
extern nsg_fns_s nsg_libc_fns;
#endif

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef char err_buf[ERR_BUF_LEN];

void *nsg_libc_malloc (size_t size, void *);
void nsg_libc_free (void *ptr, void *);
void *nsg_libc_realloc (void *ptr, size_t size, void *);
void *nsg_calloc (size_t n, size_t elem_size, nsg_allocator_s *a);
void *nsg_memset (void *p, int c, size_t n);
char *nsg_strchrnul (char *s, char ch);
char *nsg_mempcpy (void *dest, void *src, size_t n);
char *nsg_mempccpy (size_t max_bytes, void *_dest, void *_src, int c);
char *nsg_strndup (char *s, int n, nsg_allocator_s *a);
int nsg_strncmp (char *s1, char *s2, size_t n);
int nsg_strcmp(char *s1, char *s2);
char *read_file_into_str (FILE *f, nsg_allocator_s *a);
float lerp (float a, float b, float f);
int nsg_strlen (char *str);
int nsg_memcmp (void *_a, void *_b, size_t n);
void *nsg_memmove (void *_dest, void *_src, size_t n);
