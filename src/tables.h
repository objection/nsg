#pragma once

#include "under-nsg.h"

extern key_val_s NSG_TABLE_GEN_FN_DESCS[];
extern int NSG_N_TABLE_GEN_FN_DESCS;

float *gen_table (int n_harmonics, float *amps, int n_r, float phase, nsg_fns_s *fns,
		nsg_allocator_s *a);
void nsg_add_to_tables (nsg_tables_s *tables, nsg_table_s table, nsg_allocator_s *a);
