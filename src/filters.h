#pragma once

#include "under-nsg.h"

void iir_first_order_lp_filter (nsg_ctx_s *ctx, nsg_filter_state_s *filter_state,
		nsg_filter_params_s *params, float *sample);
void iir_first_order_hp_filter (nsg_ctx_s *ctx, nsg_filter_state_s *state,
		nsg_filter_params_s *params, float *sample);
void resonating_filter (nsg_ctx_s *ctx, nsg_filter_state_s *filter_state,
		nsg_filter_params_s *params, float *sample);
void bandpass_filter (nsg_ctx_s *ctx, nsg_filter_state_s *filter_state,
		nsg_filter_params_s *params, float *sample);
void balance_filter (nsg_ctx_s *ctx, nsg_filter_state_s *state,
		float cmp_samp, float cutoff_freq, float *sample);
