#pragma once

#if defined _WIN32 || defined __CYGWIN__
  #ifdef BUILDING_NSG
    #define NSG_EXPORT __declspec(dllexport)
  #else
    #define NSG_EXPORT __declspec(dllimport)
  #endif
#else
  #ifdef BUILDING_NSG
      #define NSG_EXPORT __attribute__ ((visibility ("default")))
  #else
      #define NSG_EXPORT
  #endif
#endif

#include <stdint.h>
#include <stdlib.h>
#include <assert.h> // IWYU pragma: keep

#define NSG_TWO_PI 6.283185307179586

typedef struct nsg_allocator nsg_allocator_s;
struct nsg_allocator {
	void *(*malloc) (size_t, void *ctx);
	void (*free) (void *p, void *ctx);
	void *(*realloc) (void *p, size_t size, void *ctx);
	void *ctx;
};

typedef struct nsg_fns nsg_fns_s;
struct nsg_fns {
	float (*powf) (float, float);
	float (*sinf) (float);
	float (*cosf) (float);
	float (*fabsf) (float);
	float (*sqrtf) (float);
	double (*sin) (double);
	double (*cos) (double);
	double (*sqrt) (double);
	double (*acos) (double);
};

typedef struct nsg_table nsg_table_s;
struct nsg_table {
	float *samples;
	int n_samples;
};

typedef struct nsg_tables nsg_tables_s;
struct nsg_tables {
	nsg_table_s *d;
	int n, a;
};

typedef struct nsg_osc_state nsg_osc_state_s;
struct nsg_osc_state {
	union {
		struct {
			float offset;
			float duty;
			float angle_incr;
			float phase;
		};
		struct {
			float index;
		};
	};
};

#define tick_fn_decl(name) float name (nsg_fns_s *, nsg_osc_state_s *, nsg_table_s *);
typedef tick_fn_decl (nsg_tick_fn)
#define TABLE_GEN_FN_DECL(name) float *name (int n_harms, int length, \
		nsg_fns_s *fns, nsg_allocator_s *a)
typedef TABLE_GEN_FN_DECL (nsg_table_gen_fn);
#define EASING_FN(name) float name (float f, nsg_fns_s *fns)
typedef EASING_FN (nsg_easing_fn);

enum tick_type : uint8_t {
	NTT_MATHS,
	NTT_TABLE,
};
typedef enum tick_type tick_type_e;

typedef struct nsg_tick nsg_tick_s;
struct nsg_tick {
	nsg_tick_fn *fn;
	tick_type_e type;
};

typedef struct key_val key_val_s;
struct key_val {
	char *name;
	union {
		nsg_tick_s tick;
		nsg_table_gen_fn *table_gen_fn;
		nsg_easing_fn *easing_fn;
		int an_int;
	};
};

typedef struct osc osc_s;
struct osc {
	float freq;
	float amp;
	nsg_tick_s *tick;
	nsg_osc_state_s state;
};

enum nsg_filter_type : uint8_t {
	FT_IIR_LOW_PASS,
	FT_IIR_HIGH_PASS,
	FT_RESONATING,
	FT_BANDPASS,
};
typedef enum nsg_filter_type nsg_filter_type_e;

typedef struct nsg_ctx nsg_ctx_s;
struct nsg_ctx {
	float sample_rate;
	float two_pi_over_sr;
	nsg_fns_s *fns;
};

typedef struct nsg_filter_state nsg_filter_state_s;
struct nsg_filter_state {

	union {
		float sample;
		float samples[2];
	};
};

#define NSG_FILTER_FN_DECL(name) void name (nsg_ctx_s *, nsg_filter_state_s *, nsg_filter_params_s *, float *);

typedef struct nsg_filter_params nsg_filter_params_s;
struct nsg_filter_params {
	float cutoff;
	float bandwidth;

	// Can't use a typedef here; it takes a nsg_filter_params.
	void (*fn) (nsg_ctx_s *, nsg_filter_state_s *, nsg_filter_params_s *, float *);
};

typedef NSG_FILTER_FN_DECL (nsg_filter_fn);

enum nsg_param_type : uint8_t {
	NPT_TICK = 1,
	NPT_TABLE,
	NPT_FREQ,
	NPT_AMP,
	NPT_IIR_LP_FILTER,
	NPT_IIR_HP_FILTER,
	NPT_RESONATING_FILTER,
	NPT_BP_FILTER,
};
typedef enum nsg_param_type nsg_param_type_e;

typedef struct nsg_param nsg_param_s;
struct nsg_param {
	union {
		float val;
		float freq;
		nsg_tick_s tick;
		nsg_table_s table;
		nsg_filter_params_s filter_params;
		osc_s osc;
	};
	nsg_param_type_e type;
};

enum nsg_event_op_id : uint8_t {
	NOT_STRAIGHT_VAL,
	NOT_INTERP,
	NOT_OSC,
};
typedef enum nsg_event_op_id nsg_event_op_id_e;

typedef struct nsg_event_op nsg_event_op_s;
struct nsg_event_op {
	nsg_event_op_id_e id;
	union {
		nsg_easing_fn *easing_fn;
	};
};

typedef struct nsg_event nsg_event_s;
struct nsg_event {

	// This is a union. Only start is used in nsg.c. I've put dur in
	// so that it's simpler to write the nml.
	union {

		// A 32-bit number, you'll see. I'm using floats elsewhere, so
		// I'd rather use a 32-bit number. Maybe that's not necessary.
		// Another thing I can do is make a NSG_FLOAT macro that you
		// can set to double or float.
		uint32_t start;
		float start_time;
	};
	nsg_param_s param;
	nsg_easing_fn *easing_fn;
	char arbitrary_string[64];
	nsg_event_op_s op;
};

typedef struct nsg_event_stream nsg_event_stream_s;
struct nsg_event_stream {
	nsg_event_s *d;
	int idx;
	int n;
	int a;
};

typedef struct channel_param_state channel_param_state_s;
struct channel_param_state {
	nsg_event_s *this_tick, *next_tick;
	nsg_event_s *this_table, *next_table;
	nsg_event_s *this_lp, *next_lp;
	nsg_event_s *this_hp, *next_hp;
	nsg_event_s *this_res, *next_res;
	nsg_event_s *this_bp, *next_bp;
	nsg_event_s *this_freq, *next_freq;
	nsg_event_s *this_amp, *next_amp;
};

typedef struct nsg_channel nsg_channel_s;
struct nsg_channel {

	// The member is "streams". I think that event_streams it better,
	// but I'm sick of typing it.
	nsg_event_stream_s *streams;
	int n_streams;
	nsg_osc_state_s osc_state;
	nsg_osc_state_s lerp_to_osc_state;

	nsg_filter_state_s iir_lp_filter_state;
	nsg_filter_state_s iir_hp_filter_state;
	nsg_filter_state_s resonating_filter_state;
	nsg_filter_state_s bp_filter_state;
	nsg_filter_state_s balance_filter_state;
	int n_filter_states;
	channel_param_state_s param_state;
};

typedef struct nsg_tune nsg_tune_s;
struct nsg_tune {
	nsg_channel_s *channels;
	int n_channels;
	uint32_t frame_idx;
};

[[nodiscard]] NSG_EXPORT int nsg_fill_tunes_samples (float two_pi_over_sr, float sample_rate, nsg_fns_s fns, nsg_tune_s *tune, int n_samples, float *samples);
NSG_EXPORT int nsg_free_tune (nsg_tune_s *tune, nsg_allocator_s *allocator);
[[nodiscard]] NSG_EXPORT nsg_tune_s *nsg_get_tune_from_nml (char *nml_str, char *tune_name, int sample_rate, nsg_fns_s fns, nsg_allocator_s *allocator);
[[nodiscard]] NSG_EXPORT bool nsg_you_have_all_maths_fns (nsg_fns_s *fns);
