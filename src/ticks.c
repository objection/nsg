#define _GNU_SOURCE
#include "ticks.h"
#include "under-nsg.h"

float nsg_pulse_tick (nsg_fns_s *, nsg_osc_state_s *osc_state, nsg_table_s *) {
	float r;
	if (osc_state->phase <= 2 * NSG_PI * 0.5)
		r = 1.0f;
	else
		r = -1.0f;
	return r;
}

float nsg_rand_tick (nsg_fns_s *, nsg_osc_state_s *, nsg_table_s *) {
	int the_int = rand ();
	auto r = (float) the_int / (float) RAND_MAX;
	r = r * 2.0f - 1.0f;
	return r;
}

float nsg_dsaw_tick (nsg_fns_s *, nsg_osc_state_s *osc_state, nsg_table_s *) {

	// This is what ChatGPT says. About the (1.0 / NSG_TWO_PI): "Dividing
	// 1.0 by NSG_TWO_PI yields the reciprocal of 2π2π, which is
	// equivalent to the angular resolution for one unit of phase
	// change in radians."
	//
	// When it's multiplied by the current phase, that's a scaling
	// operation. When the phase is 0 the result will be zero, and
	// when it's 2PI it will be one.
	//
	// The multiplication by two "scales the normalized value to span
	// a range from 0 to 2, which is suitable for creating a saw
	// waveform that decreases linearly from its peak to its lowest
	// point".
	//
	// And then we subtract it all from one so we end up between -1
	// and 1.
	float r = 1.0f - 2.0f * (osc_state->phase * (1.0f / NSG_TWO_PI));
	return r;
}

float nsg_usaw_tick (nsg_fns_s *, nsg_osc_state_s *osc_state, nsg_table_s *) {
	float r = (2.0f * (osc_state->phase * (1.0f / NSG_TWO_PI))) - 1.0f;
	return r;
}

float nsg_triangle_tick (nsg_fns_s *fns, nsg_osc_state_s *osc_state, nsg_table_s *) {
	float r = nsg_usaw_tick (fns, osc_state, 0);
	if (r < 0.0f)
		r = -r;
	r = 2.0f * (r - 0.5f);
	return r;
}

float nsg_sine_tick (nsg_fns_s *fns, nsg_osc_state_s *osc_state, nsg_table_s *) {
	return fns->sinf (osc_state->phase);
}

float nsg_cos_tick (nsg_fns_s *fns, nsg_osc_state_s *osc_state, nsg_table_s *) {
	float r = fns->cosf (osc_state->phase);
	return r;
}

float nsg_table_tick_lerp (nsg_fns_s *, nsg_osc_state_s *osc_state, nsg_table_s *table) {
	float frac = osc_state->index - (int) osc_state->index;
	float a = table->samples[(int) osc_state->index];
	float b = table->samples[(int) osc_state->index + 1];
	return a + frac * (b - a);
}

float nsg_table_tick_trunc (nsg_fns_s *, nsg_osc_state_s *osc_state, nsg_table_s *table) {
	return table->samples[(int) osc_state->index];
}

key_val_s NSG_TICK_FN_DESCS[] = {
	{.tick = {.fn = nsg_pulse_tick, .type = NTT_MATHS}, .name = "pulse"},
	{.tick = {.fn = nsg_rand_tick, .type = NTT_MATHS}, .name = "random"},
	{.tick = {.fn = nsg_dsaw_tick, .type = NTT_MATHS}, .name = "dsaw"},
	{.tick = {.fn = nsg_usaw_tick, .type = NTT_MATHS}, .name = "usaw"},
	{.tick = {.fn = nsg_triangle_tick, .type = NTT_MATHS}, .name = "triangle"},
	{.tick = {.fn = nsg_sine_tick, .type = NTT_MATHS}, .name = "sine"},
	{.tick = {.fn = nsg_cos_tick, .type = NTT_MATHS}, .name = "cos"},
	{.tick = {.fn = nsg_table_tick_lerp, .type = NTT_TABLE}, .name = "lerp-table"},
	{.tick = {.fn = nsg_table_tick_trunc, .type = NTT_TABLE}, .name = "trunc-table"},
};
int NSG_N_TICK_FN_DESCS = $n (NSG_TICK_FN_DESCS);
