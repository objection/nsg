#define _GNU_SOURCE
#include "easings.h"
#include "under-nsg.h"

// All these easing functions are copied from Felipe Ferreira da
// Silva's mathc library. He says it's fine to use it, though he does
// say I should mention that I did.

EASING_FN (quadratic_ease_out) {
	(void) fns;
	return -f * (f - 2.0f);
}

EASING_FN (quadratic_ease_in) {
	(void) fns;
	return f * f;
}

EASING_FN (quadratic_ease_in_out) {
	(void) fns;
	float a = 0.0f;
	if (f < 0.5f)
		a = 2.0f * f * f;
	else
		a = -2.0f * f * f + 4.0f * f - 1.0f;
	return a;
}

EASING_FN (cubic_ease_out) {
	(void) fns;
	float a = f - 1.0f;
	return a * a * a + 1.0f;
}

EASING_FN (cubic_ease_in) {
	(void) fns;
	return f * f * f;
}

EASING_FN (cubic_ease_in_out) {
	(void) fns;
	float a = 0.0f;
	if (f < 0.5f)
		a = 4.0f * f * f * f;
	else {
		a = 2.0f * f - 2.0f;
		a = 0.5f * a * a * a + 1.0f;
	}
	return a;
}

EASING_FN (quartic_ease_out) {
	(void) fns;
	float a = f -1.0f;
	return a * a * a * (1.0f - f) + 1.0f;
}

EASING_FN (quartic_ease_in) {
	(void) fns;
	return f * f * f * f;
}

EASING_FN (quartic_ease_in_out) {
	(void) fns;
	float a = 0.0f;
	if (f < 0.5f)
		a = 8.0f * f * f * f * f;
	else {
		a = f - 1.0f;
		a = -8.0f * a * a * a * a + 1.0f;
	}
	return a;
}

EASING_FN (quintic_ease_out) {
	(void) fns;
	float a = f - 1.0f;
	return a * a * a * a * a + 1.0f;
}

EASING_FN (quintic_ease_in) {
	(void) fns;
	return f * f * f * f * f;
}

EASING_FN (quintic_ease_in_out) {
	(void) fns;
	float a = 0.0f;
	if (f < 0.5f)
		a = 16.0f * f * f * f * f * f;
	else {
		a = 2.0f * f - 2.0f;
		a = 0.5f * a * a * a * a * a + 1.0f;
	}
	return a;
}

EASING_FN (sine_ease_out) {
	return fns->sinf (f * NSG_PI_2);
}

EASING_FN (sine_ease_in) {
	return fns->sinf ((f - 1.0f) * NSG_PI_2) + 1.0f;
}

EASING_FN (sine_ease_in_out) {
	return 0.5f * (1.0f - fns->cosf (f * NSG_PI));
}

EASING_FN (circular_ease_out) {
	return fns->sqrtf ((2.0f - f) * f);
}

EASING_FN (circular_ease_in) {
	return 1.0f - fns->sqrtf (1.0f - (f * f));
}

EASING_FN (circular_ease_in_out) {
	float a = 0.0f;
	if (f < 0.5f)
		a = 0.5f * (1.0f - fns->sqrtf (1.0f - 4.0f * f * f));
	else
		a = 0.5f * (fns->sqrtf (-(2.0f * f - 3.0f) * (2.0f * f - 1.0f)) + 1.0f);
	return a;
}

EASING_FN (exponential_ease_out) {
	float a = f;
	if (fns->fabsf (a) > NSG_FLT_EPSILON)
		a = 1.0f - fns->powf (2.0f, -10.0f * f);
	return a;
}

EASING_FN (exponential_ease_in) {
	float a = f;
	if (fns->fabsf (a) > NSG_FLT_EPSILON)
		a = fns->powf (2.0f, 10.0f * (f - 1.0f));
	return a;
}

EASING_FN (exponential_ease_in_out) {
	float a = f;
	if (f < 0.5f)
		a = 0.5f * fns->powf (2.0f, (20.0f * f) - 10.0f);
	else
		a = -0.5f * fns->powf (2.0f, -20.0f * f + 10.0f) + 1.0f;
	return a;
}

EASING_FN (elastic_ease_out) {
	return fns->sinf (-13.0f * NSG_PI_2 * (f + 1.0f)) * fns->powf (2.0f, -10.0f * f) + 1.0f;
}

EASING_FN (elastic_ease_in) {
	return fns->sinf (13.0f * NSG_PI_2 * f) * fns->powf (2.0f, 10.0f * (f - 1.0f));
}

EASING_FN (elastic_ease_in_out) {
	float a = 0.0f;
	if (f < 0.5f)
		a = 0.5f * fns->sinf (13.0f * NSG_PI_2 * (2.0f * f)) * fns->powf (2.0f, 10.0f * ((2.0f * f) - 1.0f));
	else
		a = 0.5f * (fns->sinf (-13.0f * NSG_PI_2 * ((2.0f * f - 1.0f) + 1.0f)) * fns->powf (2.0f, -10.0f * (2.0f * f - 1.0f)) + 2.0f);
	return a;
}

EASING_FN (back_ease_out) {
	float a = 1.0f - f;
	return 1.0f - (a * a * a - a * fns->sinf (a * NSG_PI));
}

EASING_FN (back_ease_in) {
	return f * f * f - f * fns->sinf (f * NSG_PI);
}

EASING_FN (back_ease_in_out) {
	float a = 0.0f;
	if (f < 0.5f) {
		a = 2.0f * f;
		a = 0.5f * (a * a * a - a * fns->sinf (a * NSG_PI));
	} else {
		a = (1.0f - (2.0f * f - 1.0f));
		a = 0.5f * (1.0f - (a * a * a - a * fns->sinf (f * NSG_PI))) + 0.5f;
	}
	return a;
}

EASING_FN (bounce_ease_out) {
	(void) fns;
	float a = 0.0f;
	if (f < 4.0f / 11.0f)
		a = (121.0f * f * f) / 16.0f;
	else if (f < 8.0f / 11.0f)
		a = (363.0f / 40.0f * f * f) - (99.0f / 10.0f * f) + 17.0f / 5.0f;
	else if (f < 9.0f / 10.0f)
		a = (4356.0f / 361.0f * f * f) - (35442.0f / 1805.0f * f) + 16061.0f / 1805.0f;
	else
		a = (54.0f / 5.0f * f * f) - (513.0f / 25.0f * f) + 268.0f / 25.0f;
	return a;
}

EASING_FN (bounce_ease_in) {
	return 1.0f - bounce_ease_out (1.0f - f, fns);
}

EASING_FN (bounce_ease_in_out) {
	float a = 0.0f;
	if (f < 0.5f)
		a = 0.5f * bounce_ease_in (f * 2.0f, fns);
	else
		a = 0.5f * bounce_ease_out (f * 2.0f - 1.0f, fns) + 0.5f;
	return a;
}

EASING_FN (straight) {
	(void) fns;
	return f;
}

key_val_s NSG_EASING_FN_DESCS[] = {
	{.easing_fn = quadratic_ease_out, .name = "qudratic-out"},
	{.easing_fn = quadratic_ease_in, .name = "qudratic-in"},
	{.easing_fn = quadratic_ease_in_out, .name = "qudratic-in-out"},
	{.easing_fn = cubic_ease_out, .name = "cubic-out"},
	{.easing_fn = cubic_ease_in, .name = "cubic-in"},
	{.easing_fn = cubic_ease_in_out, .name = "cubic-in-out"},
	{.easing_fn = quartic_ease_out, .name = "quartic-out"},
	{.easing_fn = quartic_ease_in, .name = "quartic-in"},
	{.easing_fn = quartic_ease_in_out, .name = "quartic-in-out"},
	{.easing_fn = quintic_ease_out, .name = "quintic-out"},
	{.easing_fn = quintic_ease_in, .name = "quintic-in"},
	{.easing_fn = quintic_ease_in_out, .name = "quintic-in-out"},
	{.easing_fn = sine_ease_out, .name = "sine-out"},
	{.easing_fn = sine_ease_in, .name = "sine-in"},
	{.easing_fn = sine_ease_in_out, .name = "sine-in-out"},
	{.easing_fn = circular_ease_out, .name = "circular-out"},
	{.easing_fn = circular_ease_in, .name = "circular-in"},
	{.easing_fn = circular_ease_in_out, .name = "circular-in-out"},
	{.easing_fn = exponential_ease_out, .name = "exp-out"},
	{.easing_fn = exponential_ease_in, .name = "exp-in"},
	{.easing_fn = exponential_ease_in_out, .name = "exp-in-out"},
	{.easing_fn = elastic_ease_out, .name = "elastic-out"},
	{.easing_fn = elastic_ease_in, .name = "elastic-in"},
	{.easing_fn = elastic_ease_in_out, .name = "elastic-in-out"},
	{.easing_fn = back_ease_out, .name = "back-out"},
	{.easing_fn = back_ease_in, .name = "back-in"},
	{.easing_fn = back_ease_in_out, .name = "back-in-out"},
	{.easing_fn = bounce_ease_out, .name = "bounce-out"},
	{.easing_fn = bounce_ease_in, .name = "bounce-in"},
	{.easing_fn = bounce_ease_in_out, .name = "bounce-in-out"},
	{.easing_fn = straight, .name = "straight"},
};

int NSG_N_EASING_FN_DESCS = $n (NSG_EASING_FN_DESCS);
