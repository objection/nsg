#define _GNU_SOURCE
#include "under-nsg.h"
#include <stdlib.h>

nsg_allocator_s nsg_libc_allocator = {
	.malloc = nsg_libc_malloc,
	.realloc = nsg_libc_realloc,
	.free = nsg_libc_free,
};

#ifdef _MATH_H
#error "IT IS DEFINED"
nsg_fns nsg_libc_fns = {
	.powf = powf,
	.sinf = sinf,
};
#endif

char *read_file_into_str (FILE *f, nsg_allocator_s *a) {
	int rc = fseek (f, 0, SEEK_END);
	if (rc == -1)
		return 0;
	u64 length = ftell (f);
	rewind (f);
	char *r = a->malloc (length, a);
	if (fread (r, 1, length, f) != length)

		// I don't think I care if there's an error, only that we get
		// all the bytes. Am I a bad man?
		return 0;
	return r;
}

int nsg_strcmp(char *s1, char *s2) {
    size_t i = 0;

    while (s1[i] || s2[i]) {
        if (s1[i] != s2[i])
            return (s1[i] > s2[i]) ? 1 : -1;
        i++;
    }

    return 0;
}

int nsg_strncmp (char *a, char *b, size_t n) {
    for (size_t i = 0; i < n; i++) {
        if (a[i] != b[i])
            return (a[i] > b[i]) ? 1 : -1;
        else if (a[i] == '\0')
            return 0;
    }
    return 0;
}

int nsg_memcmp (void *_a, void *_b, size_t n) {
    char *a = _a;
    char *b = _b;
    for (size_t i = 0; i < n; ++i) {
        if (a[i] != b[i])
            return (a[i] < b[i]) ? -1 : 1;
    }
	return 0;
}

char *nsg_mempcpy (void *_dest, void *_src, size_t n) {
	char *dest = _dest;
	char *src = _src;
	for (size_t i = 0; i < n; i++)
		dest[i] = src[i];
	return dest + n;
}

char *nsg_mempccpy (size_t max_bytes, void *_dest, void *_src, int c) {
	char *dest = _dest;
	char *src = _src;
	size_t i;
	for (i = 0; i < max_bytes; i++) {
		dest[i] = src[i];
		if (src[i] == c)
			break;
	}
	return dest + i;
}

// ChatGPT again.
void *nsg_memmove (void *_dest, void *_src, size_t n) {
    u8 *dest = _dest;
    u8 *src = _src;

    if (dest > src && dest < src + n) {

		// If the dest and src overlap (src is before dest) Copy the
		// memory block from end to start to avoid data corruption
        for (size_t i = n; i > 0; --i)
            dest[i - 1] = src[i - 1];
    } else {

        // No overlap or dest is before src, perform regular copy
        for (size_t i = 0; i < n; ++i)
            dest[i] = src[i];
    }

    return dest;
}

char *nsg_strndup (char *s, int n, nsg_allocator_s *a) {
	char *r = nsg_calloc (n + 1, sizeof *s, a);
	nsg_mempcpy (r, s, n);
	return r;
}

char *nsg_strchrnul (char *s, char ch) {
	int i;
	for (i = 0; s[i]; i++) {
		if (s[i] == ch)
			break;
	}
	return s + i;
}

int nsg_strlen (char *str) {
	int r = 0;
	while (*str++)
		r++;
	return r;
}

// I think it's fine to memset like this. ChatGPT tells me that
// for small regions there probably isn't much difference between this
// kind of memset and a compiler builtin one.
void *nsg_memset (void *_p, int c, size_t n) {
	u8 *p = _p;

	for (size_t i = 0; i < n; i++)
		p[i] = c;
	return p;
}

void *nsg_libc_malloc (size_t size, void * /* ctx */) {
	return malloc (size);
}

void nsg_libc_free (void *ptr, void * /* ctx */) {
	free (ptr);
}

void *nsg_calloc (size_t n, size_t elem_size, nsg_allocator_s *a) {
	void *r = a->malloc (n * elem_size, a->ctx);
	nsg_memset (r, 0, elem_size * n);
	return r;
}

void *nsg_libc_realloc (void *ptr, size_t size, void * /* ctx */) {
	return realloc (ptr, size);
}


float lerp (float a, float b, float f) {

	// This is a more precise version than the one commented out
	// below. The reason it's more precise might be bause it avoids
	// multiplying nearly-equal values. Or something. It's to do with
	// floating point.
	return (1.0f - f) * a + f * b;
	/* return a + (b - a) * f; */
}

