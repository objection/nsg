#pragma once

#include "under-nsg.h"

#define bar_getter_fn(name) char **name (void *a, void *user)
typedef bar_getter_fn(bar_getter_fn);

typedef struct bar_getter bar_getter_s;
struct bar_getter {
	bar_getter_fn *fn;
	int type_size;
	void *user;
	void *sentinel;
};

int n_matching (char *a, char *b);
int get_completion_idxes (int *pr, int *n_pr, char *query, int n_objs,
		void *objs, bar_getter_s *getter);
bool char_in_str_is_escaped (char *str, int pos);
int get_single_completion_match (char *str, int n_items,
		void *items, bar_getter_s *getter, char *item_kind_name,
		char err_buf[ERR_BUF_LEN], int *out);
char *get (void *items, int which, bar_getter_s *getter);
