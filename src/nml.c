#define _GNU_SOURCE

#include "misc.h"
#include "ticks.h"
#include "under-nsg.h"
#include "tables.h"
#include "filters.h"
#include "easings.h"
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

#define CREATE_FN_DESC(...) ({ \
	struct fn_desc r = (struct fn_desc) {__VA_ARGS__}; \
	r.n_args = 0; \
	for (u32 i = 0; i < $n (r.args); i++) { \
		if (!r.args[i].name) \
			break; \
		r.n_args++; \
	} \
	r; \
})
#define TOP(_block_stack) ({ NSG_ASSERT ((_block_stack)->top != -1 && (_block_stack)->d); &(_block_stack)->d[(_block_stack)->top]; })
#define MAX_VAR_LEN 256
#define DO_OR_ELSE(fn, ...) do { if (fn (__VA_ARGS__)) goto out; } while (0)
#define ADVANCE(ctx) DO_OR_ELSE (advance, ctx)
#define ADVANCE_PAST_TOKEN(ctx) DO_OR_ELSE (advance_past_token, ctx)
#define EXPECT_TOKEN(ctx, expected, msg) \
	do { \
		ADVANCE (ctx); \
		if (ctx->token.type != expected) \
			NML_ERR (ctx, msg); \
	} while (0)
#define EXPECT_VAL_OF_TYPE(ctx, bs, expected_type, val, msg_fmt, ...) \
	do { \
		DO_OR_ELSE (parse_val, ctx, bs, val); \
		if ((val)->type != expected_type) \
			NML_ERR (ctx, msg_fmt __VA_OPT__(,) __VA_ARGS__); \
	} while (0)
#define EXPECT_BAREWORD_VAL_OF_TYPE(ctx, bs, expected_type, val, msg_fmt, ...) \
	do { \
		DO_OR_ELSE (parse_bareword_val, ctx, bs, val); \
		if ((val)->type != expected_type) \
			NML_ERR (ctx, msg_fmt __VA_OPT__(,) __VA_ARGS__); \
	} while (0)
#define NML_ERR(_ctx, _fmt, ...) \
	do { \
		nml_err (_ctx, _fmt __VA_OPT__(,) __VA_ARGS__); \
		goto out; \
	} while (0)

enum {
	BLOCK_ALLOC_INCR = 4,
	BLOCK_INITIAL_ALLOC = 2,
	MAX_ARGS = 16,
};

enum nml_fixed_streams : u8 {
	NS_TICK,
	NS_TABLE,
	NS_FREQ,
	NS_AMP,
	NS_IIR_LP_FILTER,
	NS_IIR_HP_FILTER,
	NS_RESONATING_FILTER,
	NS_BP_FILTER,
	NS_N,
};
typedef enum nml_fixed_streams nml_fixed_streams_e;

#define TOKEN_X_MACRO \
	x(TT_INVALID_TOKEN) \
	x(TT_FAKE_TOKEN_ABOUT_TO_PARSE_IDENTIFIER) \
	x(TT_AMP) \
	x(TT_BANG) \
	x(TT_BLOCK_END) \
	x(TT_BLOCK_START) \
	x(TT_CHANNEL) \
	x(TT_CLOSE_BRACKET) \
	x(TT_COLON) \
	x(TT_COLON_EQUALS) \
	x(TT_COMMA) \
	x(TT_DETUNE) \
	x(TT_DOT) \
	x(TT_ENV) \
	x(TT_EOF) \
	x(TT_EQUALS) \
	x(TT_EVENT_LENGTH) \
	x(TT_EXPORT) \
	x(TT_MAKE_TABLE) \
	x(TT_MAKE_RESONATING_FILTER) \
	x(TT_RESONATING_FILTER) \
	x(TT_IIR_LP_FILTER) \
	x(TT_IIR_HP_FILTER) \
	x(TT_BP_FILTER) \
	x(TT_IMPORT) \
	x(TT_EASING_FN) \
	x(TT_MINUS) \
	x(TT_INT) \
	x(TT_FLOAT) \
	x(TT_OCTAVE) \
	x(TT_OCTAVE_DOWN) \
	x(TT_OCTAVE_UP) \
	x(TT_OPEN_BRACKET) \
	x(TT_PITCH_EVENT) \
	x(TT_PLUS) \
	x(TT_REPEAT) \
	x(TT_REST) \
	x(TT_STR) \
	x(TT_TABLE) \
	x(TT_TEMPO) \
	x(TT_TICK) \
	x(TT_TIE) \
	x(TT_TRANSPOSITION) \
	x(TT_VAR_DEF_KEYWORD) \
	x(TT_VAR_NAME)

#define x(a) a,
enum token_type : i8 { TOKEN_X_MACRO TT_N };
#undef x
typedef enum token_type token_type_e;

#define x(a) #a,
static char *token_type_strs[TT_N] = { TOKEN_X_MACRO };
#undef x

typedef struct token token_s;
struct token {
	char *s, *e;
	token_type_e type;
};

typedef struct err_info err_info_s;
struct err_info {
	int linenr, col;
	char *line;
};

typedef struct env env_s;
struct env {
	float start_level;
	float attack;
	float hold;
	float decay;
	float sustain_level;
	float release;
	bool active;
};

typedef struct var var_s;
typedef struct block_env block_env_s;
struct block_env {

	// Most of these values are stored as floats even though the
	// parser doesn't deal with floats. It'll make calculation easier.

	// Stored as actual bpm, like 120.
	float bpm;
	bool bpm_changed;

	// Stored as normalised value even though we take in the amps
	// as percentages.
	float amp;

	// Like 8 for an eighth note.
	float note_event_dur_in_beats;
	float octave;
	float transposition;
	float detune;
	var_s *vars;
	int n_vars;
	int a_vars;

	union {
		struct {
			env_s tick_env;
			env_s freq_env;
			env_s amp_env;
		};
		env_s stream_envs[NS_N];
	};

	// This is for the l command. It's meant to take effect after it's
	// set, obviously.
	float next_event_dur_in_beats;
	nsg_tick_s tick;
	nsg_easing_fn *easing_fn;
	nsg_table_s table;
	nsg_filter_params_s iir_lp_params;
	nsg_filter_params_s iir_hp_params;
	nsg_filter_params_s resonating_filter_params;
	nsg_filter_params_s bp_params;
};

typedef struct block block_s;
struct block {

	int channel_num;
	block_env_s env;
	bool failed;
	nsg_event_stream_s streams[NS_N];
	int channel_idx;

	// This is the start of the next note. It's 0 at the start, and
	// then in parse_event we add the event's dur to it.
	float next_start;
};

typedef struct block_stack block_stack_s;
struct block_stack {
	block_s *d;
	int top;
	int a;
};

#define VAL_TYPE_X_MACRO \
	x(VT_BLOCK_STACK, "block-stack") \
	x(VT_INT, "number") \
	x(VT_RHYTHM_VAL, "rhythm-val") \
	x(VT_TABLE, "table") \
	x(VT_FLOAT, "float") \
	x(VT_STR, "string")
#define x(a, ...) a,
enum val_type { VAL_TYPE_X_MACRO };
#undef x
#define x(a, b) b,
char *val_type_strs[] = { VAL_TYPE_X_MACRO };
#undef x
typedef enum val_type val_type_e;

// Potentially make this part of the key_val struct. Just add the
// val_ptr and type to it. Then you could get rid of most void
// pointers. You'd pass key_vals to the completion functions.
typedef struct arg_desc arg_desc_s;
struct arg_desc {
	char *name;
	void *val_ptr;
	val_type_e type;
};

typedef struct fn_desc fn_desc_s;
struct fn_desc {
	char *name;
	arg_desc_s args[MAX_ARGS];
	int n_args;
};

typedef struct val val_s;
struct val {
	union {
		block_stack_s block_stack;

		// Ints are floats.
		float int_val;
		float float_val;
		float rhythm_val;
		char *str;
		nsg_table_s table;
		nsg_filter_params_s filter_params;
	};
	val_type_e type;
};

typedef struct var var_s;
struct var {
	char *name;
	val_s val;
	bool exported;
};

typedef struct ctx ctx_s;
struct ctx {
	char *input;
	char *tune_name;

	// Store the channels as block. These are the channels.
	block_s *channel_blocks;
	int n_channel_blocks;

	int n_nml_channels;
	token_s token;
	int sample_rate;
	token_s last_token;
	nsg_allocator_s a;
	nsg_fns_s fns;
	nsg_tables_s tables;
};

block_env_s DEFAULT_ENV = {
	.amp = 1.0f,
	.bpm = 60.0f,
	.note_event_dur_in_beats = 1.0f,
	.octave = 2.0f,
	.transposition = 0.0f,
};

static int parse_block (ctx_s *ctx, block_stack_s *bs);
static ctx_s create_ctx (char *nml_str, char *tune_name, int sample_rate,
		nsg_fns_s fns, nsg_allocator_s *allocator);
static int parse_val (ctx_s *ctx, block_stack_s *bs, val_s *out);
static int prime_parser (ctx_s *ctx);

bar_getter_fn (key_val_getter_fn) { (void) user; return &((key_val_s *) a)->name; }
struct bar_getter key_val_getter = {
	.fn = key_val_getter_fn,
	.type_size = sizeof (key_val_s),
};

// If you want, you can fold this in together the key_val struct.
bar_getter_fn (arg_desc_getter_fn) { (void) user; return &((arg_desc_s *) a)->name; }
struct bar_getter arg_desc_getter = {
	.fn = arg_desc_getter_fn,
	.type_size = sizeof (arg_desc_s),
};

[[maybe_unused]]
static void nml_err_dont_print_token (ctx_s *ctx, char *fmt, ...) {
#define doit(fmt, ...) fprintf (stderr, fmt __VA_OPT__(,) __VA_ARGS__)
	if (ctx->tune_name)
		doit ("%s: ", ctx->tune_name);
	va_list ap;
	va_start (ap);
	vfprintf (stderr, fmt, ap);
	va_end (ap);
	doit ("\n");
#undef doit
}

static err_info_s get_err_info (ctx_s *ctx) {
	err_info_s r = {.linenr = 0};
	char *p = ctx->input;
	while (p < ctx->token.s) {
		if (*p == '\n')
			r.linenr++;
		p++;
	}
	p--;
	while (p > ctx->input && *p != '\n') {
		p--;
		r.col++;
	}
	r.line = p + 1;
	return r;
}

static void nml_err (ctx_s *ctx, char *fmt, ...) {
	auto err_info = get_err_info (ctx);
#define doit(fmt, ...) fprintf (stderr, fmt __VA_OPT__(,) __VA_ARGS__)
	if (ctx->tune_name)
		doit ("%s:", ctx->tune_name);
	doit ("%d:%d: ", err_info.linenr, err_info.col);
	va_list ap;
	va_start (ap);
	vfprintf (stderr, fmt, ap);
	va_end (ap);
	doit ("\n");
	char *line_end = nsg_strchrnul (err_info.line, '\n');
	int n_here_string = doit ( "  ---> ");
	doit ("%.*s\n", (int) (line_end - err_info.line), err_info.line);
	for (int i = 0; i < n_here_string; i++)
		doit (" ");
	for (int i = 0; i < err_info.col; i++) {
		if (err_info.line[i] == '\t')
			doit ("\t");
		else
			doit (" ");
	}
	doit ("^");
	for (int i = 0; i < ctx->token.e - ctx->token.s - 1; i++)
		doit ("~");
	doit ("\n");
#undef doit
}

static int nml_get_single_completion_match (ctx_s *ctx, char *query, int n_items,
		void *items, bar_getter_s *getter, char *item_kind_name, int *out) {
	int r = 1;
	err_buf err_buf;
	if (get_single_completion_match (query, n_items, items, getter,
				item_kind_name, err_buf, out))
		NML_ERR (ctx, "%s", err_buf);

	r = 0;
out:
	return r;
}

static int add_new_block (ctx_s *ctx, block_stack_s *stack, block_env_s *env) {
	if (!stack->a) {
		NSG_ASSERT (stack->top == -1);
		stack->a = BLOCK_INITIAL_ALLOC;
		stack->d = ctx->a.malloc (sizeof *stack->d * stack->a, ctx->a.ctx);
	} else if (stack->top >= stack->a - 1) {

		// Just add. How many blocks can we add? We also could just
		// realloc each time.
		stack->a += BLOCK_ALLOC_INCR;
		stack->d = ctx->a.realloc (stack->d, sizeof *stack->d * stack->a, ctx->a.ctx);
	}
	stack->d[++stack->top] = (block_s) {
		.env = *env,
	};
	return 0;
}

static block_s *pop_block (block_stack_s *bs) {
	block_s *r = TOP (bs);
	bs->top--;
	NSG_ASSERT (bs->top >

			// If you're at -1, that's fine, right? It means the stack
			// is empty.
			-2);
	return r;
}

static inline bool is_space (char ch) {
	return ch == '\n' || ch == '\t' || ch == ' ';
}

static inline char *get_next_non_space (char *r) {
	while (*r && is_space (*r))
		r++;
	return r;
}

static inline bool is_alpha (char c) {
    return (c >= 'a' && c <= 'z') ||
            (c >= 'A' && c <= 'Z');
}

static inline bool is_identifier_char (char ch) {

	// We don't allow numbers in identifiers because we want to allow
	// this: #make-table (h100 l1024 g"saw"), and we want to allow
	// that because this is roughly an MML parser. I'm not suggesting
	// you write your code like that, though I suspect I will.
	return is_alpha (ch) || ch == '_' || ch == '-' || ch == '\\';
}

static inline bool is_digit (char ch) {
	switch (ch) {
		case '0' ... '9':
			return 1;
	}
	return 0;
}

static int lex_pitch (token_s *token) {
	while (*token->e == '+' || *token->e == '-')
		token->e++;

	token->type = TT_PITCH_EVENT;
	return 0;
}

static void skip_white_comments_get_token_initial_char (ctx_s *ctx, token_s *t) {
	ctx->last_token = ctx->token;

	// Set the token directly so we can error in this function with
	// up-do-date token positions.
	*t = (typeof (*t)) {.e = ctx->token.e};
	t->s = get_next_non_space (ctx->token.e);

	while (*t->s == ';') {
		while (*t->s && *t->s != '\n')
			t->s++;
		t->s = get_next_non_space (t->s);
	}
	t->e = t->s + 1;
}

// This function returns a token and write errors into a buffer
// because it's used in peek-token, and I don't really want to show
// errors there. I just feel that might be confusing. Better to write
// them into a buffer and print them only when advancing.
static token_s get_token (ctx_s *ctx, token_s last_token, err_buf err_buf) {
	auto r = last_token;
	skip_white_comments_get_token_initial_char (ctx, &r);

#define SET_INVALID_TOKEN_AND_ERR_BUF(fmt, ...) \
	do { \
		snprintf (err_buf, ERR_BUF_LEN, fmt __VA_OPT__(,) __VA_ARGS__); \
		r.type = TT_INVALID_TOKEN; \
	} while (0)

	switch (*r.s) {
		case '#': {
			while (is_identifier_char (*r.e))
				r.e++;
			char buf[MAX_NAME] = {};

			// This is safe enough.
			*nsg_mempcpy (buf, r.s + 1,  r.e - (r.s + 1)) = 0;
			key_val_s long_keywords[] = {
				{.name = "make-table", .an_int = TT_MAKE_TABLE},
				{.name = "repeat", .an_int = TT_REPEAT},
			};
			int match_idx = -1;
			int rc = get_single_completion_match (buf, $n (long_keywords), long_keywords,
					&key_val_getter, "long keyword", err_buf, &match_idx);
			if (rc)
				r.type = TT_INVALID_TOKEN;

			r.type = long_keywords[match_idx].an_int;
		}
			break;
		case ':':
			if (*r.e == '=') {
				r.type = TT_COLON_EQUALS;
				r.e++;
				break;
			}
			r.type = TT_COLON;
			break;
		case '(':
			r.type = TT_OPEN_BRACKET;
			break;
		case ')':
			r.type = TT_CLOSE_BRACKET;
			break;
		case 'r':
			r.type = TT_REST;
			break;
		case 'X':
			r.type = TT_EXPORT;
			break;
		case 'I':
			r.type = TT_IMPORT;
			break;
		case '!':
			r.type = TT_BANG;
			break;
		case ',':
			r.type = TT_COMMA;
			break;
		case '"':
			r.type = TT_STR;
			while (*r.e != '"' && !char_in_str_is_escaped (r.s, r.e - r.s))
				r.e++;
			r.e++;
			if (r.e - r.s == 2)
				SET_INVALID_TOKEN_AND_ERR_BUF ("\
Your string empty. I can'r think of any use for empty strings");
			break;
		case '+':
			r.type = TT_PLUS;
			break;
		case '-':
			r.type = TT_MINUS;
			break;
		case ';':
			while (*r.e != '\n')
				r.e++;
			break;
		case '0' ... '9':
			while (is_digit (*r.e))
				r.e++;

			// Parse a float. We can't parse floats in the normal way,
			// I think, because dots are rhythm val-halfers. So we
			// expect an F at the end, and optionally a ".".
			if (*r.e == '.') {
				char *p = r.e += 1;
				while (is_digit (*p))
					p++;

				if (*p == 'F') {
					r.e = p + 1;
					r.type = TT_FLOAT;
					break;
				}
			} else if (*r.e == 'F') {
				r.type = TT_FLOAT;
				r.e++;
			}
			r.type = TT_INT;
			break;
		case '&':
			r.type = TT_TIE;
			break;
		case '{':
			r.type = TT_BLOCK_START;
			break;
		case '}':
			r.type = TT_BLOCK_END;
			break;
		case '<':
			r.type = TT_OCTAVE_DOWN;
			break;
		case '>':
			r.type = TT_OCTAVE_UP;
			break;
		case '_':
			r.type = TT_TRANSPOSITION;
			break;
		case '=':
			r.type = TT_EQUALS;
			break;
		case '$':
			if (!is_identifier_char (*r.e))
				 SET_INVALID_TOKEN_AND_ERR_BUF ("A bare \"$\" isn't a valid identifier");
			r.type = TT_VAR_NAME;
			while (*r.e && is_identifier_char (*r.e))
				r.e++;
			break;
		case '.':
			r.type = TT_DOT;
			break;
		case '`':
			r.type = TT_EASING_FN;
			break;
		case 'a' ... 'g':
			if (!lex_pitch (&r))
				break;
			break;
		case 'R':
			r.type = TT_RESONATING_FILTER;
			break;
		case 'H':
			r.type = TT_IIR_HP_FILTER;
			break;
		case 'L':
			r.type = TT_IIR_LP_FILTER;
			break;
		case 'B':
			r.type = TT_BP_FILTER;
			break;
		case 't':
			r.type = TT_TEMPO;
			break;
		case 'C':
			r.type = TT_CHANNEL;
			break;
		case 'v':
			r.type = TT_AMP;
			break;
		case 'E':
			r.type = TT_ENV;
			break;
		case 'o':
			r.type = TT_OCTAVE;
			break;
		case 'l':
			r.type = TT_EVENT_LENGTH;
			break;
		case 'D':
			r.type = TT_DETUNE;
			break;
		case 'T':
			r.type = TT_TABLE;
			break;
		case '@':
			r.type = TT_TICK;
			break;
		case 0:
			r.type = TT_EOF;
			r.e = r.s;
			break;
		default:
			SET_INVALID_TOKEN_AND_ERR_BUF ("No token begins with \"%c\"", *r.s);
			break;
	}
	return r;
#undef SET_GET_TOKEN_ERR
}

static int advance (ctx_s *ctx) {
	int r = 1;
	static err_buf err_buf;
	ctx->token = get_token (ctx, ctx->token, err_buf);
	if (ctx->token.type == TT_INVALID_TOKEN)
		NML_ERR (ctx, "Not a valid token: %s", err_buf);

	r = 0;
out:
	return r;
}

static int peek_token_type (ctx_s *ctx, token_type_e *out) {
	int r = 1;
	err_buf err_buf;
	auto next_token = get_token (ctx, ctx->token,

			// Even though we ignore errors in peek_token_type, let's
			// pass an err_buf so we won't have to bother checking for
			// NULL when writing to the err buf.
			err_buf);

	// Just return the token type. It doesn't matter if it's invalid.
	// You can deal with that on the spot.
	*out = next_token.type;

	r = 0;
	return r;
}

static int _get_int (ctx_s *ctx, int *out) {

	int r = 1;
	char *end = 0;
	int it = strtol (ctx->token.s, &end, 10);
	if (it == 0 && end == ctx->token.s)
		NML_ERR (ctx, "expected an int");

	NSG_ASSERT (end - ctx->token.s == ctx->token.e - ctx->token.s);
	*out = it;
	r = 0;
out:
	return r;
}

static int _get_float (ctx_s *ctx, float *out) {

	int r = 1;
	char *end = 0;
	float it = strtof (ctx->token.s, &end);
	if (it == 0 && end == ctx->token.s)
		NML_ERR (ctx, "expected a float");

	NSG_ASSERT (*end == 'F');
	*out = it;
	r = 0;
out:
	return r;
}

static int get_int_val (ctx_s *ctx, val_s *out) {
	int r = 1;
	int the_int;
	DO_OR_ELSE (_get_int, ctx, &the_int);

	*out = (val_s) {
		.int_val = the_int,
		.type = VT_INT
	};
	ADVANCE (ctx);
	r = 0;
out:
	return r;
}

static int get_float_val (ctx_s *ctx, val_s *out) {
	int r = 1;
	float the_float;
	DO_OR_ELSE (_get_float, ctx, &the_float);

	*out = (val_s) {
		.float_val = the_float,
		.type = VT_FLOAT
	};
	ADVANCE (ctx);
	r = 0;
out:
	return r;
}
static int get_str_val (ctx_s *ctx, val_s *out) {
	int r = 1;
	*out = (val_s) {
		.str = nsg_strndup (ctx->token.s + 1, ctx->token.e - 1 - (ctx->token.s + 1),
				&ctx->a),
		.type = VT_STR
	};
	ADVANCE (ctx);
	r = 0;
out:
	return r;
}

static block_stack_s create_block_stack (ctx_s *ctx, block_env_s env) {
	block_stack_s r = {.top = -1};
	add_new_block (ctx, &r, &env);
	return r;
}

static int create_block_stack_and_parse_block (ctx_s *ctx, block_env_s env,
		block_stack_s *out) {
	int r = 1;
	*out = create_block_stack (ctx, env);
	DO_OR_ELSE (parse_block, ctx, out);

	// parse_block advances past the }.

	pop_block (out);
	r = 0;
out:
	return r;
}

static int get_block_val (ctx_s *ctx, block_stack_s *bs, val_s *out) {
	int r = 1;
	r = 0;

	ADVANCE (ctx);

	*out = (val_s) {};
	DO_OR_ELSE (create_block_stack_and_parse_block, ctx, TOP (bs)->env,
			   &out->block_stack);

out:
	return r;
}

static int get_var_val (ctx_s *ctx, block_stack_s *bs, val_s *out) {
	int r = 1;
	block_env_s *env = &TOP (bs)->env;
	char *msg = "\
You haven't defined that variable. To create one, use :=, like \"$saw :=\"";
	if (!env->n_vars)
		NML_ERR (ctx, msg);

	NSG_ASSERT (env->a_vars && env->n_vars && env->vars);

	int match = -1;
	for (int i = 0; i < env->n_vars; i++) {
		if (!nsg_strncmp (env->vars[i].name, ctx->token.s, ctx->token.e - ctx->token.s)) {
			match = i;
			break;
		}
	}
	if (match == -1)
		NML_ERR (ctx, msg);
	*out = env->vars[match].val;
	ADVANCE (ctx);
	r = 0;
out:
	return r;
}

static int get_bareword_val (ctx_s *ctx, val_s *out) {

	int r = 1;
	auto t = &ctx->token;

	if (!is_identifier_char (*t->s))
		goto out;

	while (is_identifier_char (*t->e))
		t->e++;

	out->str = nsg_strndup (ctx->token.s, ctx->token.e - ctx->token.s, &ctx->a);
	out->type = VT_STR;
	ADVANCE (ctx);
	r = 0;
out:
	return r;
}

static int parse_bareword_val (ctx_s *ctx, block_stack_s *bs, val_s *out) {
	int r = 1;
	/* token_type next_token_type; */
	/* DO_OR_ELSE (peek_token_type, ctx, &next_token_type); */

	// FIXME: don't manipulate this directly.
	if (*ctx->token.s == '$')
		DO_OR_ELSE (get_var_val, ctx, bs, out);
	else
		DO_OR_ELSE (get_bareword_val, ctx, out);
	r = 0;
out:
	return r;
}

static bool looks_like_rhythm_val (token_type_e token_type) {
	switch (token_type) {
		case TT_DOT:
		case TT_TIE:
		case TT_INT:
			return 1;
		default:
			return 0;
	}
}

static inline int parse_rhythm_value_suffix (ctx_s *ctx, block_stack_s *bs, float *out) {
	int r = 1;
	r = 0;
	*out = 0.0f;
	auto bt = TOP (bs);
	for (;;) {
		float this_beats = bt->env.note_event_dur_in_beats;

		// We don't allow variables in rhythm_vals because we end up
		// in infinite recursion. Probably we could make it work, but
		// I don't see the point. Would you want to do this? L$this =
		// 2 a$this&$this? What's the point. Rhythm vals are actual
		// vals now; you can store them in variables, so I don't see
		// the point.
		if (ctx->token.type == TT_INT) {
			int divisor;
			DO_OR_ELSE (_get_int, ctx, &divisor);
			this_beats = 1.0 / divisor;
			ADVANCE (ctx);
		}
		while (ctx->token.type == TT_DOT) {
			this_beats /= 2.0f;
			ADVANCE (ctx);
		}
		*out += this_beats;
		if (ctx->token.type != TT_TIE)
			break;
		token_type_e next_token_type;
		DO_OR_ELSE (peek_token_type, ctx, &next_token_type);
		if (!looks_like_rhythm_val (next_token_type))
			break;
		ADVANCE (ctx);
	}
out:
	return r;
}

static int get_rhythm_val_val (ctx_s *ctx, block_stack_s *bs, val_s *out) {
	int r = 1;
	float beats;
	DO_OR_ELSE (parse_rhythm_value_suffix, ctx, bs, &beats);

	*out = (val_s) {
		.rhythm_val = beats,
		.type = VT_RHYTHM_VAL,
	};
	r = 0;
out:
	return r;
}

static int parse_and_match_bareword_item (ctx_s *ctx, block_stack_s *bs, int n_items,
		void *items, bar_getter_s *getter, char *item_kind_name, int *out) {
	int r = 1;

	val_s val;

	EXPECT_BAREWORD_VAL_OF_TYPE (ctx, bs, VT_STR, &val,
			"Needs to be a bareword, the name of a %s", item_kind_name);

	DO_OR_ELSE (nml_get_single_completion_match, ctx, val.str, n_items, items, getter,
			item_kind_name, out);

	r = 0;
out:
	return r;
}

static int advance_past_token (ctx_s *ctx) {
	ctx->token.e++;
	ctx->token.s++;
	NSG_ASSERT (*ctx->token.s);
	ctx->token.type = TT_FAKE_TOKEN_ABOUT_TO_PARSE_IDENTIFIER;
	return 0;
}

static int parse_function (ctx_s *ctx, block_stack_s *bs, fn_desc_s fn_desc) {

	int r = 1;

	bool args_mask[fn_desc.n_args] = {};

	ADVANCE (ctx);

	if (ctx->token.type != TT_OPEN_BRACKET)
		NML_ERR (ctx, "Expected open bracket");

	// This is fine, honestly. It advances just past the token we're
	// at. It's because we're about to parse barewords. In functions,
	// the parsing it different. You get barewords and then strs,
	// nums, etc. So I could write a function_advance function. I
	// could also write some generic function that takes a bunch of
	// params. The thing is that functions aren't quite parsed like
	// the rest of the file. The difference is that single characters
	// can't be tokens themselves. Only keywords and abbreviated
	// keywords can, and I think this is sensible. There might be a
	// lot of functions, and I don't want to ask the user (me) to
	// remember, eg, that "h" means "harms" in functions. Better that
	// it's "harms" and you can abbreviate it to "h" if there's
	// nothing conflicting.
	//
	// And you ask: why not make the rest of the parser behave like
	// this? The answer is if that was the case it might become pretty
	// annoying. The most important thing in an MML parser is the
	// notes. You need to be able to just type "abedfe>f&2". But you
	// can't do that in functions (maybe at some point), and so we
	// have different rules.
	ADVANCE_PAST_TOKEN (ctx);

	for (;;) {
		int match_idx;
		token_type_e next_token_type;
		DO_OR_ELSE (peek_token_type, ctx, &next_token_type);
		if (next_token_type == TT_CLOSE_BRACKET)
			break; // Advance down below.

		DO_OR_ELSE (parse_and_match_bareword_item, ctx, bs, fn_desc.n_args, fn_desc.args,
				&arg_desc_getter, "fn_desc arg name", &match_idx);

		auto arg = &fn_desc.args[match_idx];
		val_s val;
		DO_OR_ELSE (parse_val, ctx, bs, &val);

		if (val.type != arg->type) {

			if (arg->type == VT_FLOAT && val.type == VT_INT) {}
			else if (arg->type == VT_INT && val.type == VT_RHYTHM_VAL) {}

			// An exception. Rhythm vals are stored as numbers, and
			// it's not possible to distinguish a rhythm value from
			// a number when the rhythm is just a numbre.
			else
				NML_ERR (ctx, "\"%s\" must be followed by a %s", arg->name,
						val_type_strs[arg->type]);
		}
		// What's your opinion of this code, then? arg->val_ptr is a
		// void pointer.
		switch (val.type) {
			case VT_STR:         *(char **) arg->val_ptr = val.str; break;
			case VT_INT:         *(float *) arg->val_ptr = val.int_val; break;
			case VT_RHYTHM_VAL:  *(float *) arg->val_ptr = val.rhythm_val; break;
			case VT_BLOCK_STACK: *(block_stack_s *) arg->val_ptr = val.block_stack; break;
			case VT_TABLE:       *(nsg_table_s *) arg->val_ptr = val.table; break;
			case VT_FLOAT:       *(float *) arg->val_ptr = val.float_val; break;
		}
		args_mask[match_idx] = 1;
		if (ctx->token.type == TT_CLOSE_BRACKET)
			break;

	}
	int n_missing = 0;
	for (int i = 0; i < fn_desc.n_args; i++) {
		if (!args_mask[i])
			n_missing++;
	}
	if (n_missing) {
		char msg_buf[BUFSIZ];
		u32 pos = 0;
#define doit(fmt, ...) \
		pos += snprintf (msg_buf + pos, sizeof msg_buf - pos, fmt, ##__VA_ARGS__)

		doit ("Missing ");
		if (n_missing == 1)
			doit ("arg ");
		else
			doit ("args ");
		doit ("to \"%s\": ", fn_desc.name);
		for (int i = 0; i < fn_desc.n_args; i++) {
			if (!args_mask[i]) {
					doit ("\"%s\" (a %s)", fn_desc.args[i].name,
							val_type_strs[fn_desc.args[i].type]);
				if (i != fn_desc.n_args - 1)
					doit (", ");
			}
		}

#undef doit
		// This is probably buggy.
		NSG_ASSERT (pos < sizeof msg_buf);
		NML_ERR (ctx, "%s", msg_buf);
	}
	ADVANCE (ctx);

	r = 0;
out:
	return r;
}

static int parse_make_table (ctx_s *ctx, block_stack_s *bs, nsg_table_s *table) {

	int r = 1;

	// These are floats. It's because VT_INTS are floats, too. If we
	// don't make them both floats we'll get fucked up when we pass
	// the values back in parse_function. This happens because of
	// reinterpreting types as other types. Eg, int my_int;
	// *(float *) my_int = 4.3f; I'm pretty sure bad things happen
	// there, and the other way around.
	float length;
	float n_harms;
	char *gen_fn_name = 0;

	fn_desc_s fn_desc = CREATE_FN_DESC (
		.name = "table",
		.args = {
			{.name = "length", .type = VT_INT, .val_ptr = &length},
			{.name = "harms", .type = VT_INT, .val_ptr = &n_harms},
			{.name = "gen-fn", .type = VT_STR, .val_ptr = &gen_fn_name},
		},
	);

	DO_OR_ELSE (parse_function, ctx, bs, fn_desc);


	int match_idx;
	DO_OR_ELSE (nml_get_single_completion_match, ctx, gen_fn_name, NSG_N_TABLE_GEN_FN_DESCS,
				NSG_TABLE_GEN_FN_DESCS, &key_val_getter, "table gen fn", &match_idx);

	*table = (nsg_table_s) {
		.samples = NSG_TABLE_GEN_FN_DESCS[match_idx].table_gen_fn (n_harms, length,
				&ctx->fns, &ctx->a),
		.n_samples = length,
	};

	// Potentially add these to a list so we can de-dupe them if
	// they're duplicates.

	r = 0;
out:
	return r;
}

static int get_table (ctx_s *ctx, block_stack_s *bs, val_s *out) {
	int r = 1;

	nsg_table_s table;
	DO_OR_ELSE (parse_make_table, ctx, bs, &table);
	*out = (val_s) {
		.type = VT_TABLE,
		.table = table,
	};

	r = 0;
out:
	return r;
}
static int parse_val (ctx_s *ctx, block_stack_s *bs, val_s *out) {
	int r = 1;

	*out = (val_s) {};
	switch (ctx->token.type) {
		case TT_VAR_NAME:
			DO_OR_ELSE (get_var_val, ctx, bs, out);
			break;
		case TT_BLOCK_START:
			DO_OR_ELSE (get_block_val, ctx, bs, out);
			break;
		case TT_INT: {

			token_type_e next_token_type;
			DO_OR_ELSE (peek_token_type, ctx, &next_token_type);
			if (looks_like_rhythm_val (next_token_type)) {
				DO_OR_ELSE (get_rhythm_val_val, ctx, bs, out);
				break;
			}

			DO_OR_ELSE (get_int_val, ctx, out);
		}
			break;
		case TT_FLOAT:
			DO_OR_ELSE (get_float_val, ctx, out);
			break;
		case TT_STR:
			DO_OR_ELSE (get_str_val, ctx, out);
			break;
		case TT_MAKE_TABLE:
			DO_OR_ELSE (get_table, ctx, bs, out);
			break;
		default:
			if (looks_like_rhythm_val (ctx->token.type)) {
				DO_OR_ELSE (get_rhythm_val_val, ctx, bs, out);
				break;
			}

			NML_ERR (ctx, "Got %s, Not a valid value", token_type_strs[ctx->token.type]);

	}
	r = 0;
out:
	return r;
}

static int parse_plus_minus_num_val (ctx_s *ctx, block_stack_s *bs,
		float current_val, float *out) {

	int r = 1;

	// We have a last_token we can use but when I looked at this
	// function I didn't understand what it did, so it's better to be
	// clear.
	token_type_e last_token_type = -1;
	if (ctx->token.type == TT_PLUS ||ctx->token.type == TT_MINUS) {
		last_token_type = ctx->token.type;
		ADVANCE (ctx);
	}
	val_s val;
	EXPECT_VAL_OF_TYPE (ctx, bs, VT_INT, &val, "Needs to be a number, a rhythm value");

	// We should be able to just check the last token. It'd be safer
	// to keep a hold of the last token, just in case
	// get_val_from_current_token started to do something weird. But
	// it shouldn't do anything weird.
	switch (last_token_type) {
		case TT_PLUS:
			*out = current_val + val.int_val;
			break;
		case TT_MINUS:
			*out = current_val - val.int_val;
			break;
		default:
			*out = val.int_val;
			break;
	}

	r = 0;
	out:
	return r;
}

static int parse_length (ctx_s *ctx, block_stack_s *bs) {
	int r = 1;

	// Don't try to funnel this through a generic function. It's
	// similar the note event durs but not identical. There, a tie can
	// mean a slide. Just leave it.

	auto bt = TOP (bs);

	ADVANCE (ctx);
	token_type_e first_token_type = -1;
	if (ctx->token.type == TT_PLUS || ctx->token.type == TT_MINUS) {
		first_token_type = ctx->token.type;
		ADVANCE (ctx);
	}

	float new_dur = 0.0f;

	for (;;) {
		val_s val;
		EXPECT_VAL_OF_TYPE (ctx, bs, VT_INT, &val, "Needs to be a number, a rhythm value");

		float beat_fraction = 1.0 / val.int_val;
		switch (first_token_type) {
			case TT_PLUS:
				new_dur += bt->env.note_event_dur_in_beats + beat_fraction;
				break;
			case TT_MINUS:
				new_dur += bt->env.note_event_dur_in_beats - beat_fraction;
				break;
			default:
				new_dur += beat_fraction;
				break;
		}

		while (ctx->token.type == TT_DOT) {
			ADVANCE (ctx);
			new_dur /= 2.0f;
		}

		if (ctx->token.type != TT_TIE) {
			break;
		}
		ADVANCE (ctx);

	}

	bt->env.note_event_dur_in_beats = new_dur;
	r = 0;
out:
	return r;
}

static float pitch_token_to_freq (ctx_s *ctx, token_s token, int octave) {
	int semitones;

	switch (*token.s) {
		case 'c': semitones = -9; break;
		case 'd': semitones = -7; break;
		case 'e': semitones = -5; break;
		case 'f': semitones = -4; break;
		case 'g': semitones = -2; break;
		case 'a': semitones = 0; break;
		case 'b': semitones = 2; break;
		default: NSG_ASSERT (0); break;
	}

	char *p = token.s + 1;

	// Might as well let you do like "a--". Obviously you'll also be
	// able to do "a--+++---". Seems fine to me.
	while (p < token.e) {
		switch (*p) {
			case '+': semitones += 1; break;
			case '-': semitones += 1; break;
		}
		p++;
	}

	semitones += (octave - 4) * 12;

	return A4 * ctx->fns.powf (SEMITONE_RATIO, semitones);
}

static int parse_octave (ctx_s *ctx, block_stack_s *bs) {
	int r = 1;
	auto bt = TOP (bs);
	ADVANCE (ctx);
	r = 0;
out:
	DO_OR_ELSE (parse_plus_minus_num_val, ctx, bs, bt->env.octave, &bt->env.octave);
	return r;
}

static int parse_octave_up (ctx_s *ctx, block_stack_s *bs) {
	int r = 1;
	TOP (bs)->env.octave++;
	ADVANCE (ctx);
	r = 0;
out:
	return r;
}

static int parse_octave_down (ctx_s *ctx, block_stack_s *bs) {
	int r = 1;
	TOP (bs)->env.octave--;
	ADVANCE (ctx);
	r = 0;
out:
	return r;
}

static float calc_event_dur_in_seconds (float bpm, float note_event_dur_in_beats) {
	return

		// Length of a beat in seconds.
		(60.0f / bpm) * note_event_dur_in_beats;
}

static int set_easing_op (ctx_s *ctx, block_s *block, nsg_event_s *event) {
	int r = 1;

	if (!block->env.easing_fn)
		NML_ERR (ctx, "\
You're interpolating but you haven't defined an easing fn. Use, eg, \"i lerp\" ");

	event->op.id = NOT_INTERP;
	event->op.easing_fn = block->env.easing_fn;

	r = 0;
out:
	return r;
}

static int make_event (ctx_s *ctx, block_s *block, float start, nsg_param_s param,
		nsg_event_op_id_e op_id, char *arbitrary_string, nsg_event_s *out) {

	int r = 1;
	*out = (nsg_event_s) {
		.start_time = start,
		.param = param,
		.op = {.id = op_id},
	};

	// Just check every time. A little inefficient.
	if (op_id == NOT_INTERP)
		DO_OR_ELSE (set_easing_op, ctx, block, out);

	if (arbitrary_string)
		nsg_mempccpy (sizeof out->arbitrary_string, out->arbitrary_string,
				arbitrary_string, '\0');
	r = 0;
out:
	return r;
}

static bool should_make_event (block_s *block, nsg_event_stream_s *stream, nsg_param_s param) {

	// You always get ticks and amps, but you don't always get tables.
	if (param.type != NPT_TABLE && !stream->n)
		return 1;
	else {

		// We switch over the types. You can't compare a function to a
		// float, as far I know. I mean, you can, particularly since
		// it's a union. But I don't know if the results will be
		// reliable.
		switch (param.type) {
			case NPT_TABLE:

				// This is maybe a bit hacky. Tables don't get created
				// automatically like all other param types. Like,
				// amps are always set if there isn't one. I'm not
				// sure about this.
				if (!block->env.table.samples)
					return 0;
				if (!stream->n)
					return 1;

				// Probably fine. Yeah, it's fine. But we just might
				// want to put the tables in an array so we can
				// de-dupe them. Certainly if we want to include it in
				// the game.
				return nsg_memcmp (&stream->d[stream->n - 1].param, &param, sizeof param);
			default:
				return nsg_memcmp (&stream->d[stream->n - 1].param, &param, sizeof param);
		}
	}
	return 0;
}

static void add_event (ctx_s *ctx, nsg_event_stream_s *stream, nsg_event_s event) {
	grow (stream->d, stream->n, stream->a, 8, ctx->a);
	stream->d[stream->n++] = event;
}

static void add_event_to_blocks_event_stream (ctx_s *ctx, block_s *block,
		int stream_idx, nsg_event_s event) {
	add_event (ctx, &block->streams[stream_idx], event);
}

static int make_and_add_event (ctx_s *ctx, block_s *block, nsg_event_stream_s *stream,
		float start, nsg_param_s param, nsg_event_op_id_e op_id, char *arbitrary_string) {
	int r = 1;
	nsg_event_s event;
	DO_OR_ELSE (make_event, ctx, block, start, param, op_id, arbitrary_string, &event);
	add_event (ctx, stream, event);
	r = 0;
out:
	return r;
}

static int add_single_event_if_necessary (ctx_s *ctx, block_s *block, int stream_idx,
		nsg_param_s param, nsg_event_op_id_e op_id, char *arbitrary_string) {
	int r = 1;
	if (should_make_event (block, &block->streams[stream_idx], param))
		DO_OR_ELSE (make_and_add_event, ctx, block, &block->streams[stream_idx],
				block->next_start, param, op_id, arbitrary_string);
	r = 0;
out:
	return r;
}

static int parse_tick (ctx_s *ctx, block_stack_s *bs) {

	int r = 1;

	int match_idx;
	ADVANCE_PAST_TOKEN (ctx);
	DO_OR_ELSE (parse_and_match_bareword_item, ctx, bs, NSG_N_TICK_FN_DESCS,
				NSG_TICK_FN_DESCS, &key_val_getter, "tick", &match_idx);
	auto bt = TOP (bs);
	bt->env.tick = NSG_TICK_FN_DESCS[match_idx].tick;
	r = 0;
out:
	return r;
}

static int parse_amp (ctx_s *ctx, block_stack_s *bs) {

	int r = 1;
	float current_amp_percent = TOP (bs)->env.amp * 10000.0f;
	float new_amp_percent;
	ADVANCE (ctx);
	DO_OR_ELSE (parse_plus_minus_num_val, ctx, bs, current_amp_percent, &new_amp_percent);

	// Use percent so it's quicker and easier to type.
	TOP (bs)->env.amp = new_amp_percent / 10000.0f;

	r = 0;
out:
	return r;
}

#if 0
static void add_env_pair (ctx *ctx, block_stack *bs, float dest_val, float start_val,
		float next_val, float start_time, float end_time, nsg_param_type param_type,
		int stream_idx) {

	// Avoid clicks by lerping between this and the next val. Whether
	// this is a good idea, I don't know. I think it's fine.
	if (param_type == NPT_AMP && fabsf (dest_val - start_val) > 0.15f) {

		make_and_add_event (ctx, bs, start_time,
				(nsg_param) {.val = dest_val, .type = param_type},
				NOT_INTERP, stream_idx);
		make_and_add_event (ctx, bs, end_time - 0.00000001f,
				(nsg_param) {.val = next_val, .type = param_type},
				NOT_STRAIGHT_VAL, stream_idx);
		return;
	}

	make_and_add_event (ctx, bs, start_time,
			(nsg_param) {.val = dest_val, .type = param_type}, NOT_STRAIGHT_VAL,
			stream_idx);

}
#endif

static int add_env (ctx_s *ctx, block_stack_s *bs, float hold_val, float dur,
		nsg_param_type_e param_type, int stream_idx) {

	int r = 1;
	auto bt = TOP (bs);
	env_s *env = &bt->env.stream_envs[stream_idx];

	auto stream = &bt->streams[stream_idx];

	float small_number = 0.00001f;
	float start = bt->next_start;
	float end = start + dur;

	float start_level = env->start_level * hold_val;
	float sustain_level = env->sustain_level * hold_val;

	// Attack.
	DO_OR_ELSE (make_and_add_event, ctx, bt, stream, start,
			(nsg_param_s) {.val = start_level * hold_val, .type = param_type},
			NOT_INTERP, "attack");

	float new_start;
	new_start = start + calc_event_dur_in_seconds (bt->env.bpm, env->attack);
	float hold = env->hold ?: small_number;

	if (end - start < 0) {
		float val = lerp (hold_val, start_level, end - small_number - start);

		// Hold
		DO_OR_ELSE (make_and_add_event, ctx, bt, stream, end - small_number,
				(nsg_param_s) {.val = val, .type = param_type}, NOT_STRAIGHT_VAL,
				"hold; was cut off");

		return 0;
	}
	start = new_start;

	// Hold.
	DO_OR_ELSE (make_and_add_event, ctx, bt, stream, start, (nsg_param_s) {.val = hold_val,
			.type = param_type}, NOT_STRAIGHT_VAL, "hold; not cut off");
	start += calc_event_dur_in_seconds (bt->env.bpm, hold);

	if (end - start < 0)
		return 0;

	// Decay.
	DO_OR_ELSE (make_and_add_event, ctx, bt, stream, start,
			(nsg_param_s) {.val = hold_val, .type = param_type}, NOT_INTERP,
			"decay");

	new_start = start + calc_event_dur_in_seconds (bt->env.bpm, env->decay);
	if (end - new_start < 0) {
		float val = lerp (hold_val, sustain_level, end - (small_number * 2) - start);
		DO_OR_ELSE (make_and_add_event, ctx, bt, stream, start,
				(nsg_param_s) {.val = val, .type = param_type}, NOT_STRAIGHT_VAL,
				"sustain; was cut off");
		return 0;
	}
	start = new_start;

	DO_OR_ELSE (make_and_add_event, ctx, bt, stream, start,
			(nsg_param_s) {.val = sustain_level, .type = param_type}, NOT_STRAIGHT_VAL,
			"sustain; not cut off");

	r = 0;
out:
	return r;
}

static int parse_event (ctx_s *ctx, block_stack_s *bs) {
	int r = 1;

	auto bt = TOP (bs);

	auto note_token = ctx->token;

	float beats;
	if (!bt->env.tick.fn)
		NML_ERR (ctx, "\
Note doesn't have a tick set to use. Set it in the same block or an outer one");

	ADVANCE (ctx);

	if (looks_like_rhythm_val (ctx->token.type))
		parse_rhythm_value_suffix (ctx, bs, &beats);
	else
		beats = bt->env.note_event_dur_in_beats;

	float start = bt->next_start;
	float dur = calc_event_dur_in_seconds (bt->env.bpm, beats);

	if (bt->env.amp_env.active)
		DO_OR_ELSE (add_env, ctx, bs, bt->env.amp, dur, NPT_AMP, NS_AMP);
	else
		DO_OR_ELSE (add_single_event_if_necessary, ctx, bt, NS_AMP,
				(nsg_param_s) {
					.val = bt->env.amp,
					.type = NPT_AMP
				}, NOT_STRAIGHT_VAL, "parse_event amp");

	DO_OR_ELSE (add_single_event_if_necessary, ctx, bt, NS_TABLE,
			(nsg_param_s) {
				.table = bt->env.table,
				.type = NPT_TABLE
			},
			NOT_STRAIGHT_VAL, "parse_event table");

	DO_OR_ELSE (add_single_event_if_necessary, ctx, bt, NS_TICK,
				(nsg_param_s) {
					.tick = bt->env.tick,
					.type = NPT_TICK
				},
				 NOT_STRAIGHT_VAL, "parse_event tick");

	DO_OR_ELSE (add_single_event_if_necessary, ctx, bt, NS_IIR_LP_FILTER,
				(nsg_param_s) {
					.filter_params = bt->env.iir_lp_params,
					.type = NPT_IIR_LP_FILTER
				}, NOT_STRAIGHT_VAL,
				"parse_event iir lp filter");

	DO_OR_ELSE (add_single_event_if_necessary, ctx, bt, NS_IIR_HP_FILTER,
				(nsg_param_s) {
					.filter_params = bt->env.iir_hp_params,
					.type = NPT_IIR_HP_FILTER
				}, NOT_STRAIGHT_VAL,
				"parse_event iir hp filter");

	DO_OR_ELSE (add_single_event_if_necessary, ctx, bt, NS_RESONATING_FILTER,
				(nsg_param_s) {
					.filter_params = bt->env.resonating_filter_params,
					.type = NPT_RESONATING_FILTER
				}, NOT_STRAIGHT_VAL,
				"parse_event resonating filter");

	DO_OR_ELSE (add_single_event_if_necessary, ctx, bt, NS_BP_FILTER,
				(nsg_param_s) {
					.filter_params = bt->env.bp_params,
					.type = NPT_BP_FILTER
				}, NOT_STRAIGHT_VAL,
				"parse_event bp filter");

	float freq = pitch_token_to_freq (ctx, note_token, bt->env.octave);
	freq *= ctx->fns.powf (2, bt->env.detune / 1200.0f);

	if (bt->env.freq_env.active)
		DO_OR_ELSE (add_env, ctx, bs, freq, dur, NPT_FREQ, NS_FREQ);
	else {
		nsg_event_s freq_event;
		DO_OR_ELSE (make_event, ctx, bt, start,
				(nsg_param_s) {.val = freq, .type = NPT_FREQ}, NOT_STRAIGHT_VAL,
				(char []) {*note_token.s, 0}, &freq_event);
		add_event_to_blocks_event_stream (ctx, bt, NS_FREQ, freq_event);
	}

	bt->next_start += dur;

	r = 0;
out:

	return r;
}

static int parse_tempo (ctx_s *ctx, block_stack_s *bs) {
	int r = 1;
	auto bt = TOP (bs);
	ADVANCE (ctx);
	DO_OR_ELSE (parse_plus_minus_num_val, ctx, bs, bt->env.bpm, &bt->env.bpm);
	r = 0;
out:
	return r;
}

static int parse_var_def (ctx_s *ctx, block_stack_s *bs) {
	int r = 1;

	/* EXPECT_TOKEN (ctx, TT_VAR_NAME, "Not a valid variable name"); */

	var_s var = {
		.name = nsg_strndup (ctx->token.s, ctx->token.e - ctx->token.s, &ctx->a)
	};

	EXPECT_TOKEN (ctx, TT_COLON_EQUALS,  "Should be impossible to see this msg");

	ADVANCE (ctx);

	auto bt = TOP (bs);
	DO_OR_ELSE (parse_val, ctx, bs, &var.val);

	grow (bt->env.vars, bt->env.n_vars, bt->env.a_vars, 8, ctx->a);
	bt->env.vars[bt->env.n_vars++] = var;
	r = 0;
out:
	return r;
}

static bool is_num_or_plus_minus (token_type_e token_type) {
	return token_type == TT_INT || token_type == TT_PLUS || token_type == TT_MINUS;
}

static int parse_tie (ctx_s *ctx, block_stack_s *bs) {
	int r = 1;

	// We don't check that you've not done eg a t120 & e. It's easier
	// to not.

	auto bt = TOP (bs);
	auto freq_event_stream = &bt->streams[NS_FREQ];
	if (!freq_event_stream->n)
		NML_ERR (ctx, "You have a tie with no preceding note");

	ADVANCE (ctx);
	if (is_num_or_plus_minus (ctx->token.type)) {
		NSG_ASSERT (0);
		/* float current_rhythm_value = 1.0f / bt->last_note_event_dur_in_beats; */
		/* float new_rhythm_value; */
		/* if (parse_plus_minus_num_val (ctx, bs, current_rhythm_value, &new_rhythm_value)) */
		/* 	return 1; */

		/* // Use percent so it's quicker and easier to type. */
		/* bt->last_note_event_dur_in_beats += 1.0f / new_rhythm_value; */

	} else if (ctx->token.type == TT_PITCH_EVENT) {

		auto last_event = &freq_event_stream->d[freq_event_stream->n - 1];
		DO_OR_ELSE (set_easing_op, ctx, bt, last_event);

	} else
		NML_ERR (ctx, "A tie must be followed by a duration or a pitch");

	r = 0;
out:
	return r;
}

static int parse_transposition (ctx_s *ctx, block_stack_s *bs) {
	int r = 1;
	auto bt = TOP (bs);
	ADVANCE (ctx);
	DO_OR_ELSE (parse_plus_minus_num_val, ctx, bs, bt->env.transposition,
			&bt->env.transposition);
	r = 0;
out:
	return r;
}

static int parse_detune (ctx_s *ctx, block_stack_s *bs) {
	int r = 1;
	auto bt = TOP (bs);
	ADVANCE (ctx);
	DO_OR_ELSE (parse_plus_minus_num_val, ctx, bs, bt->env.detune, &bt->env.detune);
	r = 0;
out:
	return r;
}

static int append_block (ctx_s *ctx, block_s *a, block_s *b) {
	for (int i = 0; i < NS_N; i++) {
		auto a_stream = &a->streams[i];
		auto b_stream = &b->streams[i];
		if (!b_stream->n)
			continue;
		int old_a_n = a_stream->n;
		a_stream->n += b_stream->n;
		grow (a_stream->d, a_stream->n, a_stream->a, b_stream->n * 2,
				ctx->a);
		for (int j = 0; j < b_stream->n; j++) {
			a_stream->d[old_a_n + j] = b_stream->d[j];
			a_stream->d[old_a_n + j].start_time += a->next_start;
		}

	}
	a->next_start += b->next_start;
	return 0;
}

static int parse_block_level_var (ctx_s *ctx, block_stack_s *bs) {
	int r = 1;
	val_s block_val;
	token_type_e next_token_type;
	DO_OR_ELSE (peek_token_type, ctx, &next_token_type);
	if (next_token_type == TT_COLON_EQUALS) {
		DO_OR_ELSE (parse_var_def, ctx, bs);
		return 0;
	}
	DO_OR_ELSE (get_var_val, ctx, bs, &block_val);
	auto bt = TOP (bs);
	switch (block_val.type) {
		case VT_BLOCK_STACK:
			append_block (ctx, bt, &block_val.block_stack.d[0]);
			break;
		default:
			NML_ERR (ctx, "\
You've expanded a %s at block level. Only blocks  can be \
expanded by there. The rest need to be arguments to commands ",
val_type_strs[block_val.type]);
	}
	r = 0;
out:
	return r;
}

static int parse_repeat (ctx_s *ctx, block_stack_s *bs) {
	int r = 1;
	ADVANCE (ctx);
	val_s val;
	EXPECT_VAL_OF_TYPE (ctx, bs, VT_INT, &val,
			"Repeats must be followed a number (of repeats)");
	int n_repeats = val.int_val;

	EXPECT_VAL_OF_TYPE (ctx, bs, VT_BLOCK_STACK, &val,
			"Repeats must be followed by a number and then a block or block var");

	for (int i = 0; i < n_repeats; i++)
		DO_OR_ELSE (append_block, ctx, TOP (bs), &val.block_stack.d[0]);

	r = 0;
out:
	return r;
}

static int parse_channel (ctx_s *ctx, block_stack_s *bs) {
	int r = 1;
	ADVANCE (ctx);
	val_s num_val;
	EXPECT_VAL_OF_TYPE (ctx, bs, VT_INT, &num_val, "\
The channel command needs to be followed by a number. If you want to give it a \
name use a variable""Repeats must be followed a number (of repeats)");

	int channel_idx = num_val.int_val - 1;
	if (channel_idx < 0)
		NML_ERR (ctx, "Channels are counted from one, not 0");

	// Even though the user provides a channel number, we store the
	// channels in the order they come in, and qsort them at the end.
	// This is to make it easy to initialise them. It's not easy to
	// initialise sparse arrays, it seems.
	int match_idx = -1;
	for (int i = 0; i < ctx->n_channel_blocks; i++) {
		if (ctx->channel_blocks[i].channel_idx == channel_idx) {
			match_idx = 1;
			break;
		}
	}

	if (match_idx == -1) {
		match_idx = ctx->n_channel_blocks++;
		ctx->channel_blocks = ctx->a.realloc (ctx->channel_blocks,
				ctx->n_channel_blocks * sizeof *ctx->channel_blocks, ctx->a.ctx);
		ctx->channel_blocks[match_idx] = (block_s) {.channel_idx = channel_idx};
	}

	if (ctx->token.type != TT_BLOCK_START)
		NML_ERR (ctx, "\
The channel command needs to be followed by a block, an actual block, not a variable \
that expands to a block, though vars can be contained within it");
	ADVANCE (ctx);

	// You could use the main block_stack for this, but we check for
	// invalid EOF by checking if top != 0, and if this channel is
	// the last one we will encounter EOF, and top won't be -1.
	block_stack_s channel_bs;
	DO_OR_ELSE (create_block_stack_and_parse_block, ctx, TOP (bs)->env, &channel_bs);

	append_block (ctx, &ctx->channel_blocks[match_idx],

			// Not using TOP, here. Should be fine.
			&channel_bs.d[0]);
	r = 0;
out:
	return r;
}

static int parse_env (ctx_s *ctx, block_stack_s *bs) {
	int r = 1;

	auto bt = TOP (bs);

	env_s new_env = {};

	char *param_name = 0;
	fn_desc_s fn_desc = CREATE_FN_DESC (
		.name = "envelope",
		.args = {
			{.name = "parameter", .type = VT_STR, .val_ptr = &param_name},
			{.name = "start-level", .type = VT_INT, .val_ptr = &new_env.start_level},
			{.name = "attack", .type = VT_RHYTHM_VAL, .val_ptr = &new_env.attack},
			{.name = "hold", .type = VT_RHYTHM_VAL, .val_ptr = &new_env.hold},
			{.name = "decay", .type = VT_INT, .val_ptr = &new_env.decay},
			{.name = "sustain-level", .type = VT_INT, .val_ptr = &new_env.sustain_level},
			{.name = "release", .type = VT_RHYTHM_VAL, .val_ptr = &new_env.release},
		},
	);

	DO_OR_ELSE (parse_function, ctx, bs, fn_desc);

	env_s *env = &bt->env.amp_env;
	if (param_name) {

		// We probably should use a str array parallel to
		// nsg_param_type, but that enum starts from 1, and I can't be
		// bothered to figure out why. Also, I'm not sure if we'll
		// allow tick envs, because it's not clear what it would mean.
		char *param_types[NS_N] = {"tick", "amp", "freq", "table"};
		int match_idx;
		DO_OR_ELSE (nml_get_single_completion_match, ctx, param_name, $n (param_types),
				param_types, 0, "param type", &match_idx);

		switch (match_idx) {
			case 0: env = &bt->env.tick_env; break;
			case 1: env = &bt->env.amp_env; break;
			case 2: env = &bt->env.freq_env; break;
			default: IMPOSSIBLE;
		}
	}

	// You could do something to only change the stuff the user (me)
	// sets; that way you could make small changes as you go. You
	// could add a clear = false opt.

	if (env->start_level > 100 || env->start_level < 0)
		NML_ERR (ctx, "Attack levels must be betten 0 and 100");
	env->start_level /= 100.0f;
	if (env->sustain_level > 100 || env->sustain_level < 0)
		NML_ERR (ctx, "Sustain levels must be betten 0 and 100");
	env->sustain_level /= 100.0f;

	new_env.active = 1;
	*env = new_env;

	r = 0;
out:
	return r;
}

static int parse_parent_changing_block (ctx_s *ctx, block_stack_s *bs) {
	int r = 1;
	ADVANCE (ctx);
	if (ctx->token.type != TT_BLOCK_START)
		NML_ERR (ctx, "\
A bang at block level indicates the a \"parent-changing\" block and so must \
be followed by a block");

	ADVANCE (ctx);
	add_new_block (ctx, bs, &TOP (bs)->env);
	DO_OR_ELSE (parse_block, ctx, bs);
	auto block = pop_block (bs);
	append_block (ctx, TOP (bs), block);
	TOP (bs)->env = block->env;

	r = 0;
out:
	return r;
}

static int parse_import (ctx_s *ctx, block_stack_s *bs) {
	int r = 1;
	char *imported_nml_file_str = 0;
	ADVANCE (ctx);

	val_s val;
	EXPECT_VAL_OF_TYPE (ctx, bs, VT_STR, &val,
			"#import needs to be followed by a string, a path");

	FILE *f = fopen (val.str, "r");
	if (!f)
		NML_ERR (ctx, "Couldn't open your \"#import\" file %s: %m", val.str);

	imported_nml_file_str = read_file_into_str (f, &ctx->a);

	// Duplicating the whole ctx might be a little silly, considering
	// some things, like the malloc functions and maths functions
	// don't change just because you're reading a new file. Therefore,
	// feel free to split ctx into nsg_ctx and nml_ctx.
	auto imported_file_ctx = create_ctx (imported_nml_file_str,

			// The tune name; it'd be the path. Probably best to just
			// call this "path".
			val.str, ctx->sample_rate, ctx->fns, &ctx->a);
	DO_OR_ELSE (prime_parser, &imported_file_ctx);

	block_stack_s imported_file_bs;
	DO_OR_ELSE (create_block_stack_and_parse_block, &imported_file_ctx, DEFAULT_ENV,
			&imported_file_bs);

	auto bt = TOP (bs);

	// Now that we have create_block_stack_and_parse_block, we don't
	// have the top of the block_stack any more. But we know the top
	// is the first one. We'd have asserted in pop_block, else.
	auto imported_file_bt = &imported_file_bs.d[0];
	for (int i = 0; i < imported_file_bt->env.n_vars; i++) {
		auto imported_var = imported_file_bt->env.vars[i];

		// If you want, you could have a notion of
		grow (bt->env.vars, bt->env.n_vars, bt->env.a_vars, 8, ctx->a);
		bt->env.vars[bt->env.n_vars++] = imported_var;
	}

	r = 0;
out:
	if (imported_nml_file_str)
		ctx->a.free (imported_nml_file_str, &ctx->a);
	return r;
}

static int parse_export (ctx_s *ctx, block_stack_s *bs) {
	int r = 1;
	ADVANCE (ctx);

	auto bt = TOP (bs);
	DO_OR_ELSE (parse_var_def, ctx, bs);
	bt->env.vars[bt->env.n_vars - 1].exported = 1;

	r = 0;
out:
	return r;
}

static int parse_rest (ctx_s *, block_stack_s *) {
	return 0;
#if 0
	int r = 1;

	auto bt = TOP (bs);
	ADVANCE (ctx);

	float beats = bt->env.note_event_dur_in_beats;
	enum nml_fixed_streams nml_fixed_streams = NS_AMP;
	enum nsg_param_type param_type = NPT_AMP;
	env *env = &bt->env.amp_env;
	switch (ctx->token.type) {
		case TT_FREQ:
			env = &bt->env.freq_env;
			nml_fixed_streams = NS_FREQ;
			ADVANCE (ctx);
			break;

		case TT_TICK:
			env = &bt->env.tick_env;
			nml_fixed_streams = NS_TICK;
			ADVANCE (ctx);
			break;

		case TT_AMP:
			ADVANCE (ctx);
			break;
		default:
			break;

	}

	if (looks_like_rhythm_val (ctx->token.type))
		parse_rhythm_value_suffix (ctx, bs, &beats);

	switch ((int) env->active) {
		case 1: {
			if (env == &bt->env.tick_env) {
				NML_ERR (ctx, "\
You have a tick envelope active, and you're not allowed to rest with a tick \
envelope active, reason being you can't \"release\" a tick, since you have \
to end up at 0, and that would just segfault. Having said this, maybe I should \
just silently allow it, and just to a no-release-straight-cut rest");
			}
			nsg_param *previous_param = 0;
			if (bt->streams[nml_fixed_streams].n) {
				nsg_event *last_event = 0;
				last_event = &bt->streams[nml_fixed_streams].d[bt->streams[nml_fixed_streams].n - 1];
				previous_param = &last_event->param;
			}
			nsg_event first_event;
			DO_OR_ELSE (make_event, ctx, bt, calc_event_dur_in_seconds (bt->env.bpm, beats),
					*previous_param,
					NOT_INTERP,
					"rest; lerp start", &first_event);
			add_event_to_blocks_event_stream (ctx, bt, nml_fixed_streams, first_event);
			nsg_event second_event;
			DO_OR_ELSE (make_event, ctx, bt, 0,
					(nsg_param) {.val = 0, .type = param_type}, 0, "rest; lerp end",
					&second_event);
			add_event_to_blocks_event_stream (ctx, bt, nml_fixed_streams, second_event);
			break;
		}
		case 0: {
			nsg_event event;
			DO_OR_ELSE (make_event, ctx, bt, calc_event_dur_in_seconds (bt->env.bpm,
						beats), (nsg_param) {.val = 0, .type = param_type}, 0,
					"rest; no lerp", &event);
			add_event_to_blocks_event_stream (ctx, bt, nml_fixed_streams, event);
		}
			break;
	}
	r = 0;
out:
	return r;
#endif
}

static int parse_table (ctx_s *ctx, block_stack_s *bs) {
	int r = 1;

	ADVANCE (ctx);

	val_s val;
	EXPECT_VAL_OF_TYPE (ctx, bs, VT_TABLE, &val, "T expect a table, defined with G");

	auto bt = TOP (bs);
	bt->env.table = val.table;
	r = 0;
out:
	return r;
}

static int parse_easing_fn_type (ctx_s *ctx, block_stack_s *bs) {
	int r = 1;
	auto bt = TOP (bs);

	int match_idx;
	ADVANCE_PAST_TOKEN (ctx);
	DO_OR_ELSE (parse_and_match_bareword_item, ctx, bs, NSG_N_EASING_FN_DESCS,
				NSG_EASING_FN_DESCS, &key_val_getter, "easing fn", &match_idx);
	bt->env.easing_fn = NSG_EASING_FN_DESCS[match_idx].easing_fn;

	r = 0;
out:
	return r;
}

static int parse_block_start (ctx_s *ctx, block_stack_s *bs) {
	int r = 1;

	ADVANCE (ctx);
	add_new_block (ctx, bs, &TOP (bs)->env);
	DO_OR_ELSE (parse_block, ctx, bs);
	auto block = pop_block (bs);
	DO_OR_ELSE (append_block, ctx, TOP (bs), block);
	r = 0;
out:
	return r;
}

static int parse_second_order_filter (ctx_s *ctx, block_stack_s *bs,
		nsg_filter_fn *fn, nsg_filter_params_s *out) {
	int r = 1;

	fn_desc_s fn_desc = CREATE_FN_DESC (
		.name = "table",
		.args = {
			{.name = "cutoff-freq", .type = VT_FLOAT, .val_ptr = &out->cutoff},
			{.name = "bandwidth", .type = VT_FLOAT, .val_ptr = &out->bandwidth},
		},
	);
	DO_OR_ELSE (parse_function, ctx, bs, fn_desc);
	out->fn = fn;

	r = 0;
out:
	return r;
}

static int parse_bp_filter (ctx_s *ctx, block_stack_s *bs) {
	return parse_second_order_filter (ctx, bs, bandpass_filter, &TOP (bs)->env.bp_params);
}

static int parse_resonating_filter (ctx_s *ctx, block_stack_s *bs) {
	return parse_second_order_filter (ctx, bs, resonating_filter,
			&TOP (bs)->env.resonating_filter_params);
}

// Called by parse_iir_lp_filter and parse_iir_hp_filter.
static int parse_iir_filter (ctx_s *ctx, block_stack_s *bs, nsg_filter_fn *fn,
		char *name,
		nsg_filter_params_s *out) {
	int r = 1;

	fn_desc_s fn_desc = CREATE_FN_DESC (
		.name = name,
		.args = {
			{.name = "cutoff-freq", .type = VT_FLOAT, .val_ptr = &out->cutoff},
		},
	);
	DO_OR_ELSE (parse_function, ctx, bs, fn_desc);

	// This is a little bad. Really we shouldn't as the user (me) to
	// set up a function directly, just because we don't do that with
	// other things, eg ticks. With ticks, we match against an array.
	// There's no need to do that here because the filters have their
	// own commands like L for "low-pass filter". No matching needs to
	// be done. So it's not really bad; it's just inconsistent.
	out->fn = fn;

	r = 0;
out:
	return r;
}

static int parse_iir_lp_filter (ctx_s *ctx, block_stack_s *bs) {
	return parse_iir_filter (ctx, bs, iir_first_order_lp_filter,
			"lp filter", &TOP (bs)->env.iir_lp_params);
}

static int parse_iir_hp_filter (ctx_s *ctx, block_stack_s *bs) {
	return parse_iir_filter (ctx, bs, iir_first_order_hp_filter,
			"hp filter", &TOP (bs)->env.iir_hp_params);
}

static int parse_block (ctx_s *ctx, block_stack_s *bs) {
	int r = 1;

	// We don't advance here. The caller does, reason being the whole
	// program is implicitly wrapped in a block. We also don't push
	// and pop blocks here. This is so it's easy to give variables
	// envs.

	for (;;) {
		switch (ctx->token.type)  {
			case TT_IIR_LP_FILTER:        DO_OR_ELSE (parse_iir_lp_filter, ctx, bs); break;
			case TT_IIR_HP_FILTER:        DO_OR_ELSE (parse_iir_hp_filter, ctx, bs); break;
			case TT_BP_FILTER:            DO_OR_ELSE (parse_bp_filter, ctx, bs); break;
			case TT_RESONATING_FILTER:    DO_OR_ELSE (parse_resonating_filter, ctx, bs); break;
			case TT_AMP:                  DO_OR_ELSE (parse_amp, ctx, bs); break;
			case TT_BANG:      		      DO_OR_ELSE (parse_parent_changing_block, ctx, bs); break;
			case TT_BLOCK_START:          DO_OR_ELSE (parse_block_start, ctx, bs); break;
			case TT_CHANNEL:              DO_OR_ELSE (parse_channel, ctx, bs); break;
			case TT_DETUNE:               DO_OR_ELSE (parse_detune, ctx, bs); break;
			case TT_EASING_FN:            DO_OR_ELSE (parse_easing_fn_type, ctx, bs); break;
			case TT_ENV:      		      DO_OR_ELSE (parse_env, ctx, bs); break;
			case TT_EVENT_LENGTH:         DO_OR_ELSE (parse_length, ctx, bs); break;
			case TT_EXPORT:               DO_OR_ELSE (parse_export, ctx, bs); break;
			case TT_IMPORT:               DO_OR_ELSE (parse_import, ctx, bs); break;
			case TT_OCTAVE:               DO_OR_ELSE (parse_octave, ctx, bs); break;
			case TT_OCTAVE_DOWN:          DO_OR_ELSE (parse_octave_down, ctx, bs); break;
			case TT_OCTAVE_UP:            DO_OR_ELSE (parse_octave_up, ctx, bs); break;
			case TT_PITCH_EVENT:          DO_OR_ELSE (parse_event, ctx, bs); break;
			case TT_REPEAT:               DO_OR_ELSE (parse_repeat, ctx, bs); break;
			case TT_REST:                 DO_OR_ELSE (parse_rest, ctx, bs); break;
			case TT_TABLE:                DO_OR_ELSE (parse_table, ctx, bs); break;
			case TT_TEMPO:                DO_OR_ELSE (parse_tempo, ctx, bs); break;
			case TT_TICK:                 DO_OR_ELSE (parse_tick, ctx, bs); break;
			case TT_TIE:                  DO_OR_ELSE (parse_tie, ctx, bs); break;
			case TT_TRANSPOSITION:        DO_OR_ELSE (parse_transposition, ctx, bs); break;
			case TT_VAR_NAME:             DO_OR_ELSE (parse_block_level_var, ctx, bs); break;
			case TT_EOF:
										  if (bs->top != 0) {
											  NML_ERR (ctx,
													  "Found EOF while parsing block; expected %s",
													  stringify (TT_BLOCK_END));
											  goto out;
										  }
										  goto ok;
			case TT_BLOCK_END:
										  ADVANCE (ctx);
										  goto ok;
			default:
										  NML_ERR (ctx, "%s isn't a valid token here",
												  token_type_strs[ctx->token.type]);
		}
	}

ok:
	r = 0;

out:
	return r;
}

static void transform_event_start_times_into_sample_counts (nsg_event_stream_s *stream,
		int sample_rate) {
	for (int i = 0; i < stream->n; i++) {
		stream->d[i].start =

			// Might as well round?
			(stream->d[i].start_time * sample_rate) + 0.5f;
	}
}

static void insert_event (ctx_s *ctx, nsg_event_stream_s *stream, nsg_event_s event, int where) {
	grow (stream->d, stream->n, stream->a, 8, ctx->a);
    nsg_memmove (stream->d + where + 1, stream->d + where, (stream->n - where - 1) *
			sizeof *stream->d);
	stream->d[where] = event;
}

static int make_and_insert_event (ctx_s *ctx, block_s *block, nsg_event_stream_s *stream,
		float start, nsg_param_s param, nsg_event_op_id_e op_id, int where,
		char *arbitrary_string) {
	int r = 1;
	nsg_event_s event;
	DO_OR_ELSE (make_event, ctx, block, start, param, op_id, arbitrary_string, &event);
	insert_event (ctx, stream, event, where);
	r = 0;
out:
	return r;
}

[[maybe_unused]]
static int insert_amp_lerps (ctx_s *ctx, block_s *block, nsg_event_stream_s *amp_stream, float) {
	int r = 1;
	for (int i = 0; i < amp_stream->n; i++) {
		auto event = amp_stream->d + i;
		if (i == amp_stream->n - 1) {

			// This doesn't work. You need to do a lerp, probably, and
			// figure out what the final value was, and then fade from
			// there. Or you could put it in the release phase. You
			// might not want that.

#if 0
			float previous_amp = i == 0 ? 0.0f : amp_stream->d[i - 1].param.val *
				final_amp;
			make_and_add_event (ctx, amp_stream,

					// When's the end of the tune? We'll need a
					// special event to mark it.
					event->start_time + 1.0f,
					(nsg_param) {.val = previous_amp, .type = NPT_AMP},
					NOT_INTERP, "final \"release\" start of lerp");
			make_and_add_event (ctx, amp_stream,
					event->start_time + 4.0f,
					(nsg_param) {.val = 0.0f, .type = NPT_AMP},
					NOT_STRAIGHT_VAL, "final \"release\" end of lerp");
#endif
			break;
		}

		// We know we're not at the end because of the if above.
		auto next_event = event + 1;
		if (event->op.id != NOT_INTERP &&
				ctx->fns.fabsf (next_event->param.val - event->param.val) > 0.0f) {
			DO_OR_ELSE (make_and_insert_event, ctx, block, amp_stream,
					next_event->start_time - 0.01f,
					(nsg_param_s) {.val = event->param.val, .type = NPT_AMP},
					NOT_INTERP, i + 1, "click-killing auto-inserted lerp start");
			DO_OR_ELSE (make_and_insert_event, ctx, block, amp_stream,
					next_event->start_time - 0.000009f,
					(nsg_param_s) {.val = 0, .type = NPT_AMP},
					NOT_STRAIGHT_VAL, i + 2, "click-killing auto-inserted lerp start");
			/* amp_stream->d[i + 2].start_time += 0.08f; */
			/* make_and_insert_event (ctx, amp_stream, next_event->start_time - 0.05f, */
			/* 		(nsg_param) {.val = next_event->param.val, .type = NPT_AMP}, */
			/* 		NOT_STRAIGHT_VAL, i + 2, "click-killing auto-inserted lerp end"); */
			i += 2;
		}
	}
	r = 0;
out:
	return r;
}

static nsg_tune_s *parse_tune (ctx_s *ctx) {

	nsg_tune_s *r = 0;
	block_stack_s bs;
	if (create_block_stack_and_parse_block (ctx, DEFAULT_ENV, &bs))
		return 0;

	if (bs.top != -1) {
		nml_err (ctx, "You didn't close a bracket");
		return 0;
	}

	r = nsg_calloc (1, sizeof *r, &ctx->a);
	r->n_channels = ctx->n_channel_blocks;
	r->channels = nsg_calloc (r->n_channels, sizeof *r->channels, &ctx->a);
	for (int i = 0; i < ctx->n_channel_blocks; i++) {
		r->channels[i].streams = ctx->channel_blocks[i].streams;
		r->channels[i].n_streams = NS_N;
#if 0
		insert_amp_lerps (ctx, &r->channels[i].streams[2], bs.d[0].env.amp);
#endif
		each (stream, r->channels[i].n_streams, r->channels[i].streams)
			transform_event_start_times_into_sample_counts (stream, ctx->sample_rate);
	}
	ctx->a.free (bs.d, ctx->a.ctx);
	return r;
}

static ctx_s create_ctx (char *nml_str, char *tune_name, int sample_rate,
		nsg_fns_s fns, nsg_allocator_s *allocator) {
	ctx_s r = {
		.input = nml_str,
		.token.e = nml_str,
		.tune_name = tune_name,
		.sample_rate = sample_rate,
	};

	if (!allocator)
		r.a = nsg_libc_allocator;
	else
		r.a = *allocator;

	// We won't check if you've properly provided the maths functions.
	r.fns = fns;
	return r;
}

static int prime_parser (ctx_s *ctx) {

	int r = 0;

	// Skip the possible shebang line. The line number printed on
	// errors should be fine, since we're on the newline.
	if (!nsg_strncmp (ctx->input, "#!", 2)) {
		while (ctx->input && *ctx->input != '\n')
			ctx->input++;
	}

	ADVANCE (ctx);

	r = 0;
out:
	return r;
}

static void nuke_ctx (ctx_s *ctx) {
	/* ctx->a.free (ctx->channel_blocks, ctx->a.ctx); */
}

[[nodiscard]] nsg_tune_s *nsg_get_tune_from_nml (char *nml_str, char *tune_name, int sample_rate,
		nsg_fns_s fns, nsg_allocator_s *allocator) {

	auto ctx = create_ctx (nml_str, tune_name, sample_rate, fns, allocator);
	if (prime_parser (&ctx))
		return 0;
	auto r = parse_tune (&ctx);
	nuke_ctx (&ctx);

	return r;
}
