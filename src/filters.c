#define _GNU_SOURCE
#include "filters.h"

// From the Audio Programming Book. This is babby's first filter.
void iir_first_order_lp_filter (nsg_ctx_s *ctx, nsg_filter_state_s *state,
		nsg_filter_params_s *params, float *sample) {

	/* // ChatGPT says thsi is probably the cosine of theta. */
	double cos_theta = 2.0 - ctx->fns->cos (ctx->two_pi_over_sr * params->cutoff);

	// coefficient
	double coef = ctx->fns->sqrt (cos_theta * cos_theta - 1.0f) - cos_theta;
	*sample = (float) (*sample * (1.0 + coef) - state->sample *
			coef);
	state->sample = *sample;
}

// From the Audio Programming Book. This is babby's first filter.
void iir_first_order_hp_filter (nsg_ctx_s *ctx, nsg_filter_state_s *state,
		nsg_filter_params_s *params, float *sample) {
	double cos_theta = 2.0 - ctx->fns->cos (NSG_TWO_PI * params->cutoff / 48000.0f);

	// coefficient
	double coef = cos_theta - ctx->fns->sqrt (cos_theta * cos_theta - 1.0f);
	*sample = (float) (*sample * (1.0 - coef) - state->sample * coef);
	state->sample = *sample;
}

void resonating_filter (nsg_ctx_s *ctx, nsg_filter_state_s *state, nsg_filter_params_s *params,
		float *sample) {
	auto fns = ctx->fns;

	double radius = 1.0 - NSG_PI * (params->bandwidth / ctx->sample_rate);
	double two_radius = 2 * radius;
	double radius_squared = radius * radius;

	double cos_theta = (two_radius / (1.0 + radius_squared)) *
		fns->cos (ctx->two_pi_over_sr * params->cutoff);

	// He called this "scal". I guess he means scale? Does this
	// compute a scale?
	double scale = (1.0 - radius_squared) * fns->sin (fns->acos (cos_theta));

	*sample = (float) (*sample * scale + two_radius * cos_theta *

			// This will be 0 the first time around. Is that a
			// problem?
			state->samples[0] -
			radius_squared * state->samples[1]);
	state->samples[1] = state->samples[0];
	state->samples[0] = *sample;
}

void bandpass_filter (nsg_ctx_s *ctx, nsg_filter_state_s *state,
		nsg_filter_params_s *params, float *sample) {
	auto fns = ctx->fns;

	double radius = 1.0 - NSG_PI * (params->bandwidth / ctx->sample_rate);
	double two_radius = 2 * radius;
	double radius_squared = radius * radius;
	float *samples = state->samples;

	double cos_theta = (two_radius / (1.0 + radius_squared)) *
		fns->cos (ctx->two_pi_over_sr * params->cutoff);

	// He called this "scal". I guess he means scale? Does this
	// compute a scale?
	double scale = 1 - radius;

	// We're actually setting w (angular frequency) here. This is not
	// quite what I want, since I think I want to set the sample and
	// updated it in two steps, but it's calculated here, so that's
	// that.
	float w = scale * *sample + two_radius *
		cos_theta * samples[0] - radius_squared * samples[1];
	*sample = (float) (w - radius * samples[1]);

	// Update
	state->samples[1] = state->samples[0];
	state->samples[0] = w;
}

// This is a balance filter. It sets the scales the parameter "sample"
// according to cmp_samp. Can you explain how it works?
void balance_filter (nsg_ctx_s *ctx, nsg_filter_state_s *state,
		float cmp_samp, float cutoff_freq, float *sample) {
	auto fns = ctx->fns;
	auto filter_samps = state->samples;
	double cos_theta = 2.0 - fns->cos (ctx->two_pi_over_sr * cutoff_freq);
	double coef = fns->sqrt (cos_theta * cos_theta - 1.0) - cos_theta;

	filter_samps[0] = (float) ((*sample < 0 ? -(*sample) : *sample) *
			(1.0 + coef) - filter_samps[0] * coef);
	filter_samps[1] = (float) ((cmp_samp < 0 ? -cmp_samp : cmp_samp) *
			(1.0 + coef) - filter_samps[1] * coef);
	*sample *= (float) (filter_samps[0] ? filter_samps[1] / filter_samps[0] : filter_samps[1]);
}
