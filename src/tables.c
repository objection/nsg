#define _GNU_SOURCE

#include "tables.h"
#include "under-nsg.h"

static void normalise_table (float *table, int n_table) {
	float max = 0.0f;

	for (int i = 0; i < n_table; i++)
		max = MAX (table[i], max);

	if (max)  {
		for (int i = 0; i < n_table; i++)
			table[i] /= max;
	}
}

// This function does Fourier addition to produce a waveform. The n_r
// parameter is the table size. Why does this function use n_r + 2?
// And since it does this, why does it call normalise table with just
// n_r and not n_r + 2?
float *gen_table (int n_harms, float *amps, int n_r, float phase, nsg_fns_s *fns,
		nsg_allocator_s *a) {

	float *r = nsg_calloc (

			n_r + 2, sizeof *r, a);

	phase += (float) NSG_TWO_PI;

	for (int harmonic_idx = 0; harmonic_idx < n_harms; harmonic_idx++) {
		for (int i = 0; i < n_r + 2; i++) {
			float a = amps ? amps[harmonic_idx] : 1.0f;

			// He describels this as "the frequency of each harmonic".
			// w usually means "angular frequency".
			double w = (harmonic_idx

					// It's + 1, I think, because you can't really
					// multiply anything by 0.
					+ 1) * (i * NSG_TWO_PI / n_r);
			r[i] += (float) (a * fns->cosf (w + phase));
		}
	}

	normalise_table (r, n_r);
	return r;

}

TABLE_GEN_FN_DECL (gen_saw_table) {
	float *amps = a->malloc (n_harms * sizeof *amps, a);
	for (int i = 0; i < n_harms; i++)
		amps[i] = 1.0f / (i + 1);
	float *r = gen_table (n_harms, amps, length, -0.25, fns, a);
	free (amps);
	return r;
}

TABLE_GEN_FN_DECL (gen_square_table) {
	float *amps = nsg_calloc (n_harms, sizeof *amps, a);
	for (int i = 0; i < n_harms; i += 2)
		amps[i] = 1.0f / (i + 1);
	float *r = gen_table (n_harms, amps, length, -0.25, fns, a);
	free (amps);
	return r;
}

TABLE_GEN_FN_DECL (gen_triangle_table) {
	float *amps = nsg_calloc (n_harms, sizeof *amps, a);
	for (int i = 0; i < n_harms; i += 2)
		amps[i] = 1.0f / ((i + 1) * (i + 1));
	float *r = gen_table (n_harms, amps, length,

			// The Audio Programming Book missed out this argument.
			// Maybe it should be -0.25.
			0.0f,
			fns, a);
	free (amps);
	return r;
}

void nsg_add_to_tables (nsg_tables_s *tables, nsg_table_s table, nsg_allocator_s *a) {
	grow (tables->d, tables->n, tables->a, 8, *a);
	tables->d[tables->n++] = table;
}

key_val_s NSG_TABLE_GEN_FN_DESCS[] = {
	{.table_gen_fn = gen_saw_table, .name = "saw"},
	{.table_gen_fn = gen_square_table, .name = "square"},
	{.table_gen_fn = gen_triangle_table, .name = "triangle"},
};

int NSG_N_TABLE_GEN_FN_DESCS = $n (NSG_TABLE_GEN_FN_DESCS);
